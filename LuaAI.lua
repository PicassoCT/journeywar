local foo = {

	{
		name="Spawner AI: Exobiotics",
		desc="Spawns Opposing Forces that besiege you",
	},
	{
		name="Mentor AI: Tutorial",
		desc="A Tutorial for young players.",
	},
}
return foo