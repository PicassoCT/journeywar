-- 330rlexplode

return {
  ["spherespawn"] = {
  sphere = {
      air                = true,
      class              = [[CSpherePartSpawner]],
      count              = 1,
      ground             = true,
      water              = true,
      properties = {
        alpha              = 0.6,
        alwaysvisible      = true,
        color              = [[1,0.5,0]],
        expansionspeed     = 17,
        ttl                = 22,
      },
    },
	
	},

}

