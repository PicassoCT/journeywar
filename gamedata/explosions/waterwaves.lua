--lowest part of the fire

return {
  ["waterwaves"] = {
  
    groundflash = {
      air                = true,
      alwaysvisible      = true,
      circlealpha        = 0.9,
      circlegrowth       = 2.4,
      flashalpha         = 0.01,
      flashsize          = 22,
      ground             = true,
      ttl                = 27,
      water              = true,
      color = {
		[1]  = 0,
        [2]  = 0.5,
        [3]  = 1,
      },
    },
  
  
 
},


}