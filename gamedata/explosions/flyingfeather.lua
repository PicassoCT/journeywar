    return {
      ["flyingfeather"] = {
		  
	  ants2 = {
      air                = true,
      class              = [[CSimpleParticleSystem]],
      count              = 1,
      ground             = true,
      water              = false,
      properties = {
        airdrag            = 1,
		colormap           = [[1 1 1 1  1 1 1 1   1 1 1 1]],
  
        directional        = true,
        emitrot            = 5,
        emitrotspread      = 90,
            emitvector         = [[0,-1,0]],
        gravity            = [[0, 0.00000007, 0]],
        numparticles       = 5,
        particlelife       = 150,
        particlelifespread = 11,
        particlesize       = 1.2,
        particlesizespread = 4,
        particlespeed      = 0.02,
        particlespeedspread = 1.005,
        pos                = [[0, 0, 0]],
        sizegrowth         = [[0.0 0.0000000000000000001]],
        sizemod            = 0.99999999,
        texture            = [[feather]],
        useairlos          = false,
      },
    },
		
		
	  
				 
				 
				 
    },
    }
