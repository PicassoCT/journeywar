-- dirt

return {
  ["jvaryfoohit"] = {
  
  
      aexpand = {
          air                = true,
          class              = [[CBitmapMuzzleFlame]],
          count              = 1,
          ground             = true,
		
          underwater         = 0,
          water              = false,
		    
          properties = {
		    alwaysvisible      = true,
		    useairlos          = false,	
			colormap           = [[0.7 0.7 0.70 0.03 	0.7 0.7 0.70  0.03	 0.7 0.7 0.70  0.01		0 0 0 0.01	]],
			dir                = [[dir]],
            frontoffset        = 0,
            fronttexture       = [[varyfoostrikeii]],
            length             = 5,
            sidetexture        = [[citdronegrad]],
            size               = 25,
            sizegrowth         = -0.25,
            ttl                = 10,
          },
        }
  
  
   
  },

}

