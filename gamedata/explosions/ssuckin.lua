    return {
      ["ssuckin"] = {


	

	             bark= {
          air                = true,
          class              = [[CSimpleParticleSystem]],
          count              = 1,
          ground             = true,
          water              = false,
          properties = {
            airdrag            = 1,
            colormap           = [[0 0.8 1 0.1     0 0.5 1 0.1 ]],
            directional        = true,
            emitrot            = 0,
            emitrotspread      = 0,
            emitvector         = [[dir]],
            gravity            = [[0, 0, 0]],
            numparticles       = 1,
            particlelife       = 8,
            particlelifespread = 0,
            particlesize       = 1,
            particlesizespread = 0,
            particlespeed      = 3,
            particlespeedspread = 0.5,
            pos                = [[0, 0, 0]],
            sizegrowth         = [[0.6 r.001]],
            sizemod            = 0.5,
            texture            = [[alphabluedot]],
            useairlos          = false,
          },
        },
     	
				 
    },
    }
