    -- trail_ar2
     
    return {
      ["conairexaust"] = {

           
     
        exaust = {
          air                = true,
          class              = [[CBitmapMuzzleFlame]],
          count              = 1,
          ground             = true,
          underwater         = 0,
          water              = true,
          properties = {
            colormap           = [[  0.2 0.6 1 .01   1 0.7 0.25 .01         0 0 0 0]],
            dir                = [[dir]],
            frontoffset        = 0,
            fronttexture       = [[laserendblue]],
            length             = 3,
            sidetexture        = [[laserendblue]],
            size               = 3,
            sizegrowth         = 1,
            ttl                = 4,
          },
        },
   
     
     
    },
    }
