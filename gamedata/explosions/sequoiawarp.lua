    return {
      ["sequoiawarp"] = {

		
	 electric2 = {
      air                = true,
      class              = [[heatcloud]],
      count              = 1,
      ground             = true,
      water              = true,
      properties = {
        alwaysvisible      = true,
        heat               = 11,
        heatfalloff        = 1.3,
        maxheat            = 5,
        pos                = [[0, 0, 0]],
        size               =1,
        sizegrowth         = 5,
        speed              = [[0, 0, 0]],
        texture            = [[laserendgreen]],
      },
    },	
				 
				 
    },
    }
