-- trail_ar2

return {
  ["cplasmaproj"] = {
  

  
	  Flashsfx = {
      air                = true,
      class              = [[CSimpleParticleSystem]],
      count              = 1,
      ground             = true,
      water              = false,
      properties = {
        airdrag            = 1,
          colormap           = [[0.6 1 1 0.045  0.6 1 1 0.035  0.5 0.9 1 0.035   0.1 0.05 0.8 0.025 ]],
        directional        = true,
        emitrot            = 0,
        emitrotspread      = 0,
        emitvector         = [[dir]],
        gravity            = [[0, 0, 0]],
        numparticles       = 1,
        particlelife       = 6,
        particlelifespread = 0,
        particlesize       = 0.5,
        particlesizespread = 1,
        particlespeed      = 3.05,
        particlespeedspread = 0.00,
        pos                = [[0, 0, 0]],
        sizegrowth         = [[0.22s0.22]],
        sizemod            = 1.0,
        texture            = [[plasma.png]],
        useairlos          = false,
      },
    },
	
	
 
  


               
    
      
 
       
 
       
  },
 
}