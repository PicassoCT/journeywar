    return {
      ["factorywarp"] = {

		
	 electric2 = {
      air                = true,
      class              = [[heatcloud]],
      count              = 1,
      ground             = true,
      water              = true,
      properties = {
        alwaysvisible      = true,
        heat               = 11,
        heatfalloff        = 1.5,
        maxheat            = 5,
        pos                = [[0, 0, 0]],
        size               =0.5,
        sizegrowth         = 2,
        speed              = [[0, 0, 0]],
        texture            = [[laserendblue]],
      },
    },	
				 
				 
    },
    }
