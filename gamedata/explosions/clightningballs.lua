-- redlight

return {
  ["clightball"] = {
  	 


	 lensflare = {
	 
      air                = true,
      class              = [[CSimpleParticleSystem]],
      count              = 1,
      ground             = true,
      properties = {
        airdrag            = 1,
        alwaysvisible      = true,
         colormap           = [[0.05 0.35 0.95  0.01	0.05 0.35 0.95  0.015  	0.05 0.35 0.95  0.01	0.05 0.35 0.95  0.015 	0.05 0.35 0.95  0.01   0.05 0.35 0.95  0.015 	0.05 0.35 0.95  0.001		  ]],
        directional        = true,
        emitrot            = 0,
        emitrotspread      = 0,
        emitvector         = [[0,-0.1,0]],
        gravity            = [[0, -0.1   , 0]],
        numparticles       = 1,
        particlelife       = 205,
        particlelifespread = 0,
        particlesize       = 15 ,
        particlesizespread = 7,
        particlespeed      = 70,
        particlespeedspread = 3,
        pos                = [[0, 160, 0]],
        sizegrowth         = 1.000000000000001,
        sizemod            =  0.997	,
        texture            = [[bluenovaexplo]],
        useairlos          = false,
      },
    },

	 lensflare = {
	 
      air                = true,
      class              = [[CSimpleParticleSystem]],
      count              = 3,
      ground             = true,
      properties = {
        airdrag            = 1,
        alwaysvisible      = true,
         colormap           = [[0.05 0.35 0.95  0.01	0.05 0.35 0.95  0.015  	0.05 0.35 0.95  0.01	0.05 0.35 0.95  0.015 	0.05 0.35 0.95  0.01   0.05 0.35 0.95  0.015 	0.05 0.35 0.95  0.001		  ]],
        directional        = true,
        emitrot            = 0,
        emitrotspread      = 0,
        emitvector         = [[0,-0.1,0]],
        gravity            = [[0, -0.1   , 0]],
        numparticles       = 1,
        particlelife       = 205,
        particlelifespread = 0,
        particlesize       = 15 ,
        particlesizespread = 7,
        particlespeed      = 70,
        particlespeedspread = 3,
        pos                = [[0, 160, 0]],
        sizegrowth         = 1.000000000000001,
        sizemod            =  0.997	,
        texture            = [[flare]],
        useairlos          = false,
      },
    },
	
	
	}
  }


