-- dirt

return {
  ["blueglow"] = {
    
    whiteglow = {
      air                = true,
      class              = [[heatcloud]],
      count              = 2,
      ground             = true,
      water              = true,
      properties = {
        alwaysvisible      = true,
        heat               = 10,
        heatfalloff        = 1.1,
        maxheat            = 15,
        pos                = [[0 r5 r-5,25, 0 r5 r-5]],
        size               = 1,
        sizegrowth         = 8,
        speed              = [[0 r1 r-1, 0 r1 r-1, 0 r1 r-1]],
        texture            = [[laserendblue]],
      },
    },
	    whiteglows = {
      air                = true,
      class              = [[heatcloud]],
      count              = 2,
      ground             = true,
      water              = true,
      properties = {
        alwaysvisible      = true,
        heat               = 10,
        heatfalloff        = 1.1,
        maxheat            = 15,
        pos                = [[0 r5 r-5, 50, 0 r5 r-5]],
        size               = 1,
        sizegrowth         = 8,
        speed              = [[0 r1 r-1, 0 r1 r-1, 0 r1 r-1]],
        texture            = [[laserendblue]],
      },
    },
  
  
  
  },
  

}

