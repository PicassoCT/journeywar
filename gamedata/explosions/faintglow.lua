-- dirt

return {
  ["faintglow"] = {
    
    whiteglow = {
      air                = true,
      class              = [[heatcloud]],
      count              = 1,
      ground             = true,
      water              = true,
      properties = {
        alwaysvisible      = true,
        heat               = 1,
        heatfalloff        = 0.1,
        maxheat            = 1,
        pos                = [[0,-30,0]],
        size               = 15,
        sizegrowth         = 8,
        speed              = [[0,0,0]],
        texture            = [[laserendblue]],
      },
    },
  
  
  
  },
  

}

