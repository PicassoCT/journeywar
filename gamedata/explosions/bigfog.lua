-- dirt

return {
	["bigfog"] = {
		
		
		fog3 = {
			
			
			air = true,
			class = [[CBitmapMuzzleFlame]],
			count = 1,
			ground = true,
			underwater = true,
			water = true,
			properties = {
				colormap = [[
				0 0 0 0.0
				0.9 0.9 0.9 0.1125	
				0.9 0.9 0.9 0.1125	
				0.9 0.9 0.9 0.1125	
				0.9 0.9 0.9 0.1125
				0.9 0.9 0.9 0.1125	
				0.9 0.9 0.9 0.1125	
				0.9 0.9 0.9 0.1125
				0 0 0 0.0]],
				dir = [[0r-0.0125r0.0125,0.75r0.1,0r-0.125r0.125]],
				
				frontoffset =0,
				fronttexture = [[new_dirtb]],
				length = 132,
				sidetexture = [[]],
				size = 280,
				sizegrowth = 1.125000000005,
				ttl = 300,
			},
		
			
			
		},
		
	},
	
}