    -- trail_ar2
     
    return {
      ["sniperlazzor"] = {
	
	    bitmapmuzzleflame = {	
      air                = true,
      class              = [[CBitmapMuzzleFlame]],
      count              = 1,
      ground             = true,
      underwater         = 1,
      water              = true,
      properties = {
        colormap           = [[0.1 0.2 0.5 0.004	0.1 0.2 0.5 0.004		0.1 0.2 0.5 0.0038]],
        dir                = [[dir]],
        frontoffset        = 0.05,
        fronttexture       = [[0]],
        length             = 600,
        sidetexture        = [[TouchGround]],
        size               = 24,
        sizegrowth         = 0,
        ttl                = 5,
      },
    },
	
	
	  
    },
    }
