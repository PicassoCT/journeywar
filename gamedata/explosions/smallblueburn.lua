-- beamimpact

return {
  ["smallblueburn"] = {
  

    pop1 = {
      air                = true,
      class              = [[heatcloud]],
      count              = 1,
      ground             = true,
      water              = true,
      properties = {
        alwaysvisible      = true,
        heat               = 10,
        heatfalloff        = 10,
        maxheat            = 10,
        pos                = [[0, 5, 0]],
        size               = 0.02,
        sizegrowth         = 0,
        speed              = [[0, 0, 0]],
        texture            = [[bluedot]],
      },
    },

  }

}

