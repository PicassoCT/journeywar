-- trail_ar2

return {
	["Ricochet"] = {
		
		bitmapmuzzleflame = {
			air = true,
			class = [[CBitmapMuzzleFlame]],
			count = 4,
			ground = true,
			underwater = 1,
			water = true,
			properties = {
				colormap = [[0.1 0.8 0.8 0.01	0.2 0.7 0.9 0.01	0.0 0.0 0.0 0.01]],
				dir = [[0.1r0.9r-0.9,0.2r0.8,0.1r0.9r-0.9]],
				frontoffset = 0,
				fronttexture = [[]],
				length = 12,
				sidetexture = [[ricochet2]],
				size = 0.1,
				sizegrowth = 12.01,
				ttl = 15,
			},
		},
		sparkstretch = {
			air = true,
			class = [[CBitmapMuzzleFlame]],
			count = 3,
			ground = true,
			underwater = 1,
			water = true,
			properties = {
				colormap = [[0.1 0.8 0.8 0.01	0.2 0.7 0.9 0.01	0.0 0.0 0.0 0.01]],
				dir = [[dir]],
				frontoffset = 0,
				fronttexture = [[]],
				length = 20,
				sidetexture = [[ricochet]],
				size = 4,
				sizegrowth = 6.01,
				ttl = 10,
			},
		},
		
		sparkcore = {
			air = true,
			class = [[CBitmapMuzzleFlame]],
			count = 3,
			ground = true,
			underwater = 1,
			water = true,
			properties = {
				colormap = [[0.1 0.8 0.8 0.01	0.2 0.7 0.9 0.01	0.0 0.0 0.0 0.01]],
				dir = [[0.1r0.9r-0.9,0.2r0.8,0.1r0.9r-0.9]],
				frontoffset = 0,
				fronttexture = [[]],
				length = 5,
				sidetexture = [[ricochet]],
				size = 2,
				sizegrowth = 6.01,
				ttl = 10,
			},
		},
		
		
		
	},
	
}