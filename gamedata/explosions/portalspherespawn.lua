-- 330rlexplode

return {
  ["portalspherespawn"] = {
  sphere = {
      air                = true,
      class              = [[CSpherePartSpawner]],
      count              = 1,
      ground             = true,
      water              = true,
      properties = {
        alpha              = 0.6,
        alwaysvisible      = true,
        color              = [[0,0.5,1]],
        expansionspeed     = 17,
        ttl                = 22,
      },
    },
	
	},

}

