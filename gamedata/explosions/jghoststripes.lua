-- dirt

return {
	["jghoststripe"] = {
		
		outberbolt = {
			
			air = true,
			class = [[CBitmapMuzzleFlame]],
			count = 1,
			ground = true,
			underwater = 1,
			water = true,
			properties = {
				colormap = [[ 0.0 0 0 0		0.5 0.5 1 0.004	 0 0.9 0.9 0.006	0.0 0.9 0.9 0.004 0.0 0.4 0.4 0.00 0.16 0.4 0.4 0.00]],
				dir = [[dir]],
				frontoffset = 0,
				fronttexture = [[]],
				length = 32,
				sidetexture = [[citdronegrad]],
				size = -6,
				sizegrowth = 0.18,
				ttl = 15,
			}
		},
		
		
	}
}