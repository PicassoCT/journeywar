    -- trail_ar2
     
    return {
      ["ccastermini"] = {
	  
	   bitmapmuzzleflame = {
          air                = true,
          class              = [[CBitmapMuzzleFlame]],
          count              = 1,
          ground             = true,
          underwater         = 1,
          water              = true,
          properties = {
              colormap           = [[0.5 0.5 0.5 0.45         0.1 0.3 0.5 0.1  ]],
            dir                = [[dir]],
            frontoffset        = 8.42,
            fronttexture       = [[portalMini]],
            length             = 3,
            sidetexture        = [[]],
            size               = 65,
            sizegrowth         = 0.01,
            ttl                = 5,
			alwaysvisible=true,
          },
        },
	 
	
	
	
	  
    },
    }
