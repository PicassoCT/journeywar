-- megapartflash
-- shotgun
-- smallvulcan
-- beammuzzle
-- purplemuzzle
-- large_muzzle_flash_fx
-- yellowmuzzelflash
-- vulcan

return {
  ["orangeray"] = {
  
 
								bitmapmuzzleflame = {
								  air                = true,
								  class              = [[CBitmapMuzzleFlame]],
								  count              = 1,
								  ground             = true,
								  underwater         = 1,
								  water              = true,
								  properties = {
									colormap           = [[0.9 0.8 0.6 0.035	1 0.9 0.7 0.035	0.9 0.8 0.6  0.01]],
									dir                = [[dir]],
									frontoffset        = 0,
									fronttexture       = [[]],
									length             = 420,
									sidetexture        = [[lightray]],
									size               = 42,
									sizegrowth         = 0.001,
									ttl                = 5,
								  },
								},
							   
							  

				},
}

