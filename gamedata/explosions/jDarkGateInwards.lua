
    return {
      ["jdarkgateinwards"] = {
	  
	   bitmapmuzzleflame = {
          air                = true,
          class              = [[CBitmapMuzzleFlame]],
          count              = 1,
          ground             = true,
          underwater         = 1,
          water              = true,
          properties = {
              colormap           = [[  0.5 0.9 0.9 0.001   0.25 0.6 0.6 0.225      0.5 0.5 0.5 0.525         0.1 0.3 0.5 0.001  ]],
            dir                = [[dir]],
            frontoffset        = 0,
            fronttexture       = [[inGateTex]],
            length             = 0,
            sidetexture        = [[]],--outRay
            size               = 20,
            sizegrowth         = 0.0001,
            ttl                = 125,
			alwaysvisible=true,
          },
        },	  
    },
    }
