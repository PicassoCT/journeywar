
    return {
      ["jdarkgateoutwards"] = {
	  
	   bitmapmuzzleflame = {
          air                = true,
          class              = [[CBitmapMuzzleFlame]],
          count              = 1,
          ground             = true,
          underwater         = 1,
          water              = true,
          properties = {
              colormap           = [[  0.5 0.9 0.9 0.001   0.5 0.9 0.9  0.0125      0.1 0.3 0.5 0.015         0.1 0.3 0.5 0.001  ]],
            dir                = [[dir]],
            frontoffset        = 0,
            fronttexture       = [[outGateTex]],
            length             = 82,
            --sidetexture        = [[outRay]],--outRay
            sidetexture        = [[shotgunside]],--outRay
            size               = 15,
            sizegrowth         = 0.6,
            ttl                = 45,
			alwaysvisible=true,
          },
        },
	 
	
	
	
	  
    },
    }
