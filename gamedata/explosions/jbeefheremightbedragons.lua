

    return {
      ["jbeefheremightbedragons"] = {
		  
	  ants2 = {
      air                = true,
      class              = [[CSimpleParticleSystem]],
      count              = 1,
      ground             = true,
      water              = false,
      properties = {
        airdrag            = 1,
		colormap           = [[1 1 1 1   1 1 1 1]],
		directional        = false,
        emitrot            = 65,
        emitrotspread      = 55,
        emitvector         = [[0,1,0]],
        gravity            = [[0, -0.03, 0]],
        numparticles       = 1,
        particlelife       = 15,
        particlelifespread = 45,
        particlesize       = [[0.05]],
        particlesizespread = 0.2,
        particlespeed      = 3,
        particlespeedspread = 3,
        pos                = [[r-0.5 r0.5, 1 r2, r-0.5 r0.5]],
        sizegrowth         = 1.000000000000001,
        sizemod            = 0.5,
        texture            = [[grassh]],
    useairlos          = false,
      },
	      
    },
		
		
	  
				 
				 
				 
    },
    }
