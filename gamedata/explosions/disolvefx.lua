return {
  ["disolvefx"] = {
  
 
		lightshape = {
		  air                = true,
		  class              = [[CBitmapMuzzleFlame]],
		  count              = 3,
		  ground             = true,
		  underwater         = 1,
		  water              = true,
		  properties = {
			colormap           = [[ 1 1 1 0.1	 
									1 1 1 0.1
									1 1 1 0.1 
									1 1 1 0.1	
			]],
			dir                = [[0,1,1]],
			frontoffset        = 1,
			fronttexture       = [[disolvefx]],
			length             = 50,
			sidetexture        = [[]],
			size               = 25,
			sizegrowth         = 0,
			ttl                = 120,
		  },
		},
							   
		
		darkshape = {
		  air                = true,
		  class              = [[CBitmapMuzzleFlame]],
		  count              = 3,
		  ground             = true,
		  underwater         = 1,
		  water              = true,
		  properties = {
			colormap           = [[ 0 0 0 0.01	 
									0 0 0 0.01
									0 0 0 0.01 
									0 0 0 0.01	
			]],
			dir                = [[0,1,1]],
			frontoffset        = 1,
			fronttexture       = [[disolvefx]],
			length             = 50,
			sidetexture        = [[]],
			size               = 25,
			sizegrowth         = 0,
			ttl                = 120,
		  },
		},					  

				},
}

