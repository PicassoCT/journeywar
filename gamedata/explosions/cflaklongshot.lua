-- trail_ar2

return {
  ["cflaklongshot"] = {
  

  
  
  

               
       
    Flash = {
      air                = true,
      class              = [[CSimpleParticleSystem]],
      count              = 1,
      ground             = true,
      water              = false,
      properties = {
        airdrag            = 1,
        colormap           = [[0.5 0.9 1 0.01     0.6 1 1 0.01    0.6 1 1 0.01  0.5 0.9 1 0.025   0.1 0.05 0.8 0.025   ]],
        directional        = true,
        emitrot            = 0,
        emitrotspread      = 0,
        emitvector         = [[dir]],
        gravity            = [[0, 0, 0]],
        numparticles       = 1,
        particlelife       = 6,
        particlelifespread = 0,
        particlesize       = 10.5,
        particlesizespread = 1,
        particlespeed      = 0.05,
        particlespeedspread = 0.05,
        pos                = [[0, 0, 0]],
        sizegrowth         = [[0.6 ]],
        sizemod            = 1.0,
        texture            = [[longshot]],
        useairlos          = false,
      },
    },

 
       
 
       
  },
 
}