--lowest part of the fire

return {
  ["volcanolight"] = {
  
    groundflash = {
      air                = true,
      alwaysvisible      = true,
      circlealpha        = 0.5,
      circlegrowth       = 8,
      flashalpha         = 0.02,
      flashsize          = 42,
      ground             = true,
      ttl                = 47,
      water              = true,
      color = {
        [1]  = 0.9,
        [2]  = 0.2,
        [3]  = 0,
      },
    },
  
  
 
},


}