-- dirt

return {
	["fog"] = {
		dirtg = {
			
			
			air = true,
			class = [[CBitmapMuzzleFlame]],
			count = 1,
			ground = true,
			underwater = true,
			water = true,
			properties = {
				colormap = [[
				0.9 0.9 0.9 0.01225	
				0.9 0.9 0.9 0.0625	
				0.9 0.9 0.9 0.125	
				0.9 0.9 0.9 0.1253	
				0.9 0.9 0.9 0.1255	
				0.9 0.9 0.9 0.1258	
				0.9 0.9 0.9 0.1255	
				0.9 0.9 0.9 0.1253	
				0.9 0.9 0.9 0.125		
				0 0 0 0.0]],
				dir = [[0r-0.0125r0.0125,0.75r0.1,0r-0.125r0.125]],
				
				frontoffset = -0.00065,
				fronttexture = [[new_dirtb]],
				length = 32,
				sidetexture = [[]],
				size = 80,
				sizegrowth = 1.000000005,
				ttl = 120,
			},

			
			
		},		
		fog2 = {
			
			
			air = true,
			class = [[CBitmapMuzzleFlame]],
			count = 1,
			ground = true,
			underwater = true,
			water = true,
			properties = {
				colormap = [[
				0 0 0 0.0	
				0.9 0.9 0.9 0.1225	
				0.9 0.9 0.9 0.1125
				0.9 0.9 0.9 0.1725	
				0.9 0.9 0.9 0.1125	
				0.9 0.9 0.9 0.1125	
				0 0 0 0.0]],
				dir = [[0r-0.0125r0.0125,0.75r0.1,0r-0.125r0.125]],
				
				frontoffset =0,
				fronttexture = [[new_dirtb]],
				length = 132,
				sidetexture = [[]],
				size = 190,
				sizegrowth = -0.5000000005,
				ttl = 150,
			},
		
			
			
		},
		
  
		
	},
	
}