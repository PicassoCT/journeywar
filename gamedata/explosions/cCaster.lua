    -- trail_ar2
     
    return {
      ["ccaster"] = {
	  
	   bitmapmuzzleflame = {
          air                = true,
          class              = [[CBitmapMuzzleFlame]],
          count              = 1,
          ground             = true,
          underwater         = 1,
          water              = true,
          properties = {
              colormap           = [[0.5 0.5 0.5 0.45         0.1 0.3 0.5 0.1  ]],
            dir                = [[dir]],
            frontoffset        = 1.42,
            fronttexture       = [[portal]],
            length             = 3,
            sidetexture        = [[]],
            size               = 135,
            sizegrowth         = 0.01,
            ttl                = 5,
			alwaysvisible=true,
          },
        },
	 
	
	
	
	  
    },
    }
