-- gawtail

return {
  ["gawtail"] = {
    pop2 = {
      air                = true,
      class              = [[heatcloud]],
      count              = 2,
      ground             = true,
      water              = true,
      properties = {
        heat               = 10,
        heatfalloff        = 5,
        maxheat            = 15,
        pos                = [[0, 0, 0]],
        size               = [[5 r-1.5]],
        sizegrowth         = 0.9,
        speed              = [[0, 0, 0]],
        texture            = [[explo]],
      },
    },
  },

}

