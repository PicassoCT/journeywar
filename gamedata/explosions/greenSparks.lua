    -- trail_ar2
     
    return {
      ["greenSparks"] = {
	  
 greenSparks = {
      air                = true,
      class              = [[CSimpleParticleSystem]],
      count              = 1,
      ground             = true,
      water              = false,
      properties = {
        airdrag            = 1,
        colormap           = [[0.1 0.9 0.31 .001   0.1 0.9 0.031 .01		0 0 0 0.01]],
        directional        = true,
        emitrot            = 0,
        emitrotspread      = 40,
        emitvector         = [[0 r0.1 r-0.1,1,0 r0.1 r-0.1]],
        gravity            = [[0, -0.00000007, 0]],
        numparticles       = 1,
        particlelife       = 550,
        particlelifespread = 11,
        particlesize       = 2,
        particlesizespread = 0,
        particlespeed      = 0.002,
        particlespeedspread = 1.005,
        pos                = [[0, 0, 0]],
        sizegrowth         = [[0.0 0.0000000000000003]],
        sizemod            = 1.0000000029,
        texture            = [[Flake]],
        useairlos          = false,
      },
    },
		
	  
    },
    }
