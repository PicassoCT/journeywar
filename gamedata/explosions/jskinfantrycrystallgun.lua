return {
	["jskinfantrygun"] = {
		
		alwaysvisible = true,
		usedefaultexplosions = false,
		
		outberbolt = {
			
			
			air = true,
			class = [[CBitmapMuzzleFlame]],
			count = 1,
			ground = true,
			underwater = 1,
			water = true,
			properties = {
				colormap = [[1 0.6 0.05 0.01 	1 0.4 0.025 0.005 1 0.9 0.05 0.01 	 1 0.4 0.025 0.005 	 	 1 0.6 0.05 0.01 	1 0.4 0.025  0.0025 	 		1 0.6 0 0.001  1 0.4 0.025 0.002 		1 0.6 0 0.001 	 0 0 0 0.01]],
				dir = [[dir]],
				frontoffset = 0,
				fronttexture = [[empty]],
				length = -35,
				sidetexture = [[citdronegrad]],
				size = 2,
				sizegrowth = 0.01,
				ttl = 28,
			}
		},
		
	}
}