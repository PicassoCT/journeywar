

return {
	["gluebuff"] = {
		
		lightpillars = {			
			air = true,
			class = [[CBitmapMuzzleFlame]],
			
			count = 3,
			ground = true,
			underwater = false,
			water = true,
			properties = {
				colormap = [[1 1 1 0.05 	0.3 1 0 0.04 	 0 0.8 0.9 0.03 	 	0 0.8 0.9 0.0]],
				dir = [[0r-1r1,-0.1r-0.7,0r-1r1]],
				frontoffset = 0.75,
				fronttexture = [[jshieldgeom4]],
				length = 85,
				sidetexture = [[pulseshot]],
				size = 15.5,
				sizegrowth = 0.125125,
				ttl = 65,
			},			
		},
		
		glue = {			
			air = true,
			class = [[CBitmapMuzzleFlame]],
			count = 3,
			ground = true,
			underwater = false,
			water = true,
			properties = {
			colormap = [[1 1 1 0.05 	0.3 1 0 0.04 	 0 0.8 0.9 0.03 	 	0 0.8 0.9 0.0]],
				dir = [[0r-1r1,-0.1r-0.7,0r-1r1]],
				frontoffset = 0.75,
				fronttexture = [[jshieldgeom4]],
				length = 85,
				sidetexture = [[pulseshot]],
				size = 15.5,
				sizegrowth = 0.25125,
				ttl = 65,
			},			
		},
		
		
	},
}