

return {
	["parallyzebuff"] = {
		
		lightpillars = {			
			air = true,
			class = [[CBitmapMuzzleFlame]],
			count = 17,
			ground = true,
			underwater = false,
			water = true,
			properties = {
				colormap = [[0.1 0.5 1 0.05 	 0.1 0.5 1 0.05 	 0.0 0.5 1 0.05 0 0	0.5 0.01]],
				dir = [[0r-1r1,0.1r0.7,0r-1r1]],
				frontoffset = 0.5,
				fronttexture = [[jshieldgeom5]],
				length = 175,
				sidetexture = [[pulseshot]],
				size = 15.5,
				sizegrowth = -0.5125,
				ttl = 65,
			},			
		}
		
		
	},
}