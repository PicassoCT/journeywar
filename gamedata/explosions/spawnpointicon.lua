return {
  ["spawnpointicon"] = {
  
 
								bitmapmuzzleflame = {
								  air                = true,
								  class              = [[CBitmapMuzzleFlame]],
								  count              = 1,
								  ground             = true,
								  underwater         = 1,
								  water              = true,
								  properties = {
									colormap           = [[1 0.4 0 0.005	  1 0.4 0 0.004	  1 0.4 0 0.003    1 0.4 0 0.001	]],
									dir                = [[0,-1,0]],
									frontoffset        = 0,
									fronttexture       = [[spawnpointtop]],
									length             = 500,
									sidetexture        = [[spawnpointside]],
									size               = 25,
									sizegrowth         = 0.01,
									ttl                = 5,
								  },
								        useairlos          = true,
								},
							   
							  

				},
}

