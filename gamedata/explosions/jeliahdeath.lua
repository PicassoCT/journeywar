return {
	["jeliadeath"] = {
		

		rainbow = {
			air = true,
			class = [[CSimpleParticleSystem]],
			count = 5,
			ground = true,
			properties = {
				airdrag = 0.8,
				alwaysvisible = true,
				colormap = [[
				0.1 0.8 0.9 0.01
				0.9 0.1 0.1 0.05
				0.7 0.5 0.1 0.05
				0.1 0.8 0.9 0.05
				0.1 0.1 0.1 0.001]],
				directional = true,
				emitrot = 1,
				emitrotspread = 1,
					emitvector = [[0,0.3r0.7,0]],
				gravity = [[r0.25r-0.25, r0.3r-0.05, r0.25r-0.25]],
				numparticles = 1,
				particlelife = 50,
				particlelifespread = 150,
				particlesize = 6,
				particlesizespread = 12,
				particlespeed = 10,
				particlespeedspread = 5,
				pos = [[0r10r-10, 2, 0r10r-10]],
				sizegrowth = [[0.0 0.0000000000000000001]],
				sizemod = 0.99999999,
				texture = [[jeliahbutterfly]],
				useairlos = false,
			},
		},
		rainbow2 = {
			air = true,
			class = [[CSimpleParticleSystem]],
			count = 5,
			ground = true,
			properties = {
				airdrag = 0.8,
				alwaysvisible = true,
				colormap = [[
				0.9 0.1 0.1 0.01
				0.1 0.1 0.9 0.05
				0.5 0.3 0.5 0.05
				0.9 0.1 0.1 0.05
				0.1 0.1 0.1 0.001]],
				directional = true,
				emitrot = 1,
				emitrotspread = 1,
				emitvector = [[r0.5r-1,0.3r0.7,r0.5r-1]],
				gravity = [[r0.25r-0.25, r0.01r-0.005, r0.25r-0.25]],
				numparticles = 1,
				particlelife = 50,
				particlelifespread = 150,
				particlesize = 6,
				particlesizespread = 12,
				particlespeed = 10,
				particlespeedspread = 5,
				pos = [[0, 2, 0]],
				sizegrowth = [[0.0 0.0000000000000000001]],
				sizemod = 0.99999999,
				texture = [[jeliahbutterfly]],
				useairlos = false,
			},
		},	
		rainbow3 = {
			air = true,
			class = [[CSimpleParticleSystem]],
			count = 5,
			ground = true,
			properties = {
				airdrag = 0.8,
				alwaysvisible = true,
				colormap = [[
				0.9 0.1 0.1 0.01
				0.1 0.6 0.9 0.05
				0.7 0.5 0.1 0.05
				0.9 0.1 0.1 0.05
				0.1 0.1 0.1 0.001]],
				directional = true,
				emitrot = 1,
				emitrotspread = 1,
					emitvector = [[r0.5r-1,0.3r0.7,r0.5r-1]],
				gravity = [[r0.25r-0.25, r0.01r-0.005, r0.25r-0.25]],
				numparticles = 1,
				particlelife = 50,
				particlelifespread = 150,
				particlesize = 6,
				particlesizespread = 12,
				particlespeed = 10,
				particlespeedspread = 5,
				pos = [[0, 2, 0]],
				sizegrowth = [[0.0 0.0000000000000000001]],
				sizemod = 0.99999999,
				texture = [[jeliahbutterfly]],
				useairlos = false,
			},
		},
		
		
		
	}		
}