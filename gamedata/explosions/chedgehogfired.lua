-- dirt

return {
	["chedgehogfired"] = {
		

		poof01 = {
			air = true,
			class = [[CSimpleParticleSystem]],
			count = 2,
			ground = true,
			properties = {
				airdrag = 0.8,
				colormap = [[0.5 0.6 0.9 .01 0.3 0.4 1 .01	0 0 0 0.01]],
				directional = false,
				emitrot = 15,
				emitrotspread = 90,
				emitvector = [[0, 1, 0]],
				gravity = [[0, 0.05, 0]],
				numparticles = 12,
				particlelife = 30,
				particlelifespread = 250,
				particlesize = 55,
				particlesizespread = 3,
				particlespeed = 9,
				particlespeedspread = 3,
				pos = [[0, 2, 0]],
				sizegrowth = 0,
				sizemod = 1.0,
				texture = [[4explo]],
				useairlos = false,
			},
		},
	sparkout = {
			air = true,
			class = [[CSimpleParticleSystem]],
			count = 2,
			ground = true,
			water = false,
			properties = {
				airdrag = 1,
				
				colormap = [[0.5 0.6 0.9 0.001 0.67 0.45 0.9 0.01		0 0 0 0.01]],
				directional = true,
				emitrot = 22,
				emitrotspread = 5,
				emitvector = [[0, 1, 0]],
				gravity = [[0, 0, 0]],
				numparticles = 1,
				particlelife = 20,
				particlelifespread = 0,
				particlesize = 44.2,
				particlesizespread = 4,
				particlespeed = 0.00,
				particlespeedspread = 0,
				pos = [[0, 0, 0]],
				sizegrowth = 0,
				sizemod = 1.10001,
				texture = [[expandingstrobe]],
				useairlos = false,
			},
		},	
	  electricarcs1 = {
      air                = true,
      class              = [[CSimpleParticleSystem]],
      count              = 1,
      ground             = true,
      properties = {
        airdrag            = 0.8,
        alwaysvisible      = true,
						colormap = [[0.5 0.6 0.9 0.04  0.3 0.4 1 0.01	0.3 0.4 1  0.01]],

        directional        = true,
        emitrot            = 45,
        emitrotspread      = 32,
        emitvector         = [[0, -1, 0]],
        gravity            = [[0, -0.25, 0]],
        numparticles       = 15,
        particlelife       = 20,
        particlelifespread = 15,
        particlesize       = 10,
        particlesizespread = 0,
        particlespeed      = 5,
        particlespeedspread = 5,
        pos                = [[0, 2, 0]],
        sizegrowth         = 1,
        sizemod            = 1.0,
        texture            = [[lightening]],
        useairlos          = false,
      },
    },
		
		
	},
	
}