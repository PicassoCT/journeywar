-- blood_spray

return {
  ["waterspray"] = {
    flare12 = {
      air                = true,
      class              = [[CBitmapMuzzleFlame]],
      ground             = false,
      water              = true,
      properties = {
       colormap           = [[0.2 0.8 1 0.01  	0 0 0 0.01]],
        dir                = [[-1 r2, -1 r2, -1 r2]],
        frontoffset        = 0,
        fronttexture       = [[watersplat]],
        length             = 2,
        sidetexture        = [[watersplat]],
        size               = 2,
        sizegrowth         = 16,
        ttl                = 18,
      },
    },
	
	flare22 = {
      air                = true,
      class              = [[CBitmapMuzzleFlame]],
      ground             = false,
      water              = true,
      properties = {
        colormap           = [[0.2 0.8 1 0.01  	0 0 0 0.01]],
        dir                = [[-1 r2, -1 r2, -1 r2]],
        frontoffset        = 0,
        fronttexture       = [[watersplat]],
        length             = 2,
        sidetexture        = [[watersplat]],
        size               = 3,
        sizegrowth         = 16,
        ttl                = 35,
      },
    },
	
	 spark = {
      air                = true,
      class              = [[CSimpleParticleSystem]],
      count              = 1,
      ground             = true,
      water              = true,
      properties = {
        airdrag            = 1,
        colormap           = [[0.2 0.8 1 0.01  	0 0 0 0.01]],
        directional        = true,
        emitrot            = 0,
        emitrotspread      = 40,
        emitvector         = [[0,1,0]],
        gravity            = [[0, -0.17, 0]],
        numparticles       = 2,
        particlelife       = 27,
        particlelifespread = 17,
        particlesize       = 0.5,
        particlesizespread = 0,
        particlespeed      = 2,
        particlespeedspread = 3,
        pos                = [[0, 0, 0]],
        sizegrowth         = 1.001,
        sizemod            = 1.0,
        texture            = [[Flake]],
        useairlos          = false,
      },
    },
	
	waterdrop = {
      air                = true,
      class              = [[CSimpleParticleSystem]],
      count              = 1,
      ground             = true,
      water              = true,
      properties = {
        airdrag            = 0.0001,
        colormap           = [[0.2 0.8 1 0.01  	0 0 0 0.01]],
        directional        = true,
        emitrot            = 0,
        emitrotspread      = 40,
        emitvector         = [[0,1,0]],
        gravity            = [[0, -0.37, 0]],
        numparticles       = 5,
        particlelife       = 90,
        particlelifespread = 11,
        particlesize       = 0.5,
        particlesizespread = 0,
        particlespeed      = 0.002,
        particlespeedspread = 1.005,
        pos                = [[0, 0, 0]],
        sizegrowth         =  1.0000000000001,
        sizemod            = 0.9999,
        texture            = [[firesparks]],
        useairlos          = false,
      },
    },
	
  },

}
