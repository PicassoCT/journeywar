    -- trail_ar2
     
    return {
      ["jbuildingexaust"] = {

           
     
        exaust = {
          air                = true,
          class              = [[CBitmapMuzzleFlame]],
          count              = 1,
          ground             = true,
          underwater         = 0,
          water              = true,
          properties = {
            colormap           = [[  0.9 0.9 0.3 .01   0.6 0.9 0.0 .02  0.6 0.9 0.0 .01     0.4 0.6 0.1 0.01   0 0 0 0]],
            dir                = [[0,-1,0]],
            frontoffset        = 0,
            fronttexture       = [[laserendgreen]],
            length             = 150,
            sidetexture        = [[beamrifle]],
            size               = 25,
            sizegrowth         = 1,
            ttl                = 15,
          },
        },
   
     
     
    },
    }
