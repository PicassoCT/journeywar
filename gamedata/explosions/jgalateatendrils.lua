-- missile_explosion

return {
  ["jgalateatend"] = {

  
		
		    tendrils = {
          air                = false,
          class              = [[CBitmapMuzzleFlame]],
          count              = 2,
          ground             = false,
          underwater         = true,
          water              = true,
          properties = {
         colormap           = [[  0.03 0.03 0.03 0.004 	0.05 0.05 0.05 0.005  0.05 0.05 0.05 0.02 
											0.05 0.05 0.05 0.03 	0.05 0.05 0.05 0.04  	0.05 0.05 0.05 0.001 
											0.03 0.03 0.03 0.004 	0.05 0.05 0.05 0.005  0.05 0.05 0.05 0.02 
											0.05 0.05 0.05 0.03 	0.05 0.05 0.05 0.04  	0.05 0.05 0.05 0.001 
											0.05 0.05 0.0000]],
		    dir                = [[0r0.1,0r1,0r0.1]],
            frontoffset        = 0,
            fronttexture       = [[jgalateatendrils]],--redexplo
            length             = 1,
            sidetexture        = [[]],
            size               = 95,
            sizegrowth         = -0.45,
            ttl                =75,
          },
        },
		
		
		   tendrils2 = {
          air                = false,
          class              = [[CBitmapMuzzleFlame]],
          count              = 2,
          ground             = false,
          underwater         = true,
          water              = true,
          properties = {
                  colormap           = [[  0.03 0.03 0.03 0.004 	0.05 0.05 0.05 0.005  0.05 0.05 0.05 0.02 
											0.05 0.05 0.05 0.03 	0.05 0.05 0.05 0.04  	0.05 0.05 0.05 0.001 
											0.03 0.03 0.03 0.004 	0.05 0.05 0.05 0.005  0.05 0.05 0.05 0.02 
											0.05 0.05 0.05 0.03 	0.05 0.05 0.05 0.04  	0.05 0.05 0.05 0.001 
											0.05 0.05 0.0000]],
            dir                = [[0r0.1,0r1,0r0.1]],
            frontoffset        = 0,
            fronttexture       = [[jgalateatendrils]],--redexplo
            length             = 1,
            sidetexture        = [[]],
            size               = 15,
            sizegrowth         = 2.15,
            ttl                =75,
          },
        },
		
		
		
		
   
		
	 

  },

}

