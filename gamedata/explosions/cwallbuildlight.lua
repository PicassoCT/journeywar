-- missile_explosion

return {
  ["cwallbuildlight"] = {
   
  groundflash = {
      air                = true,
      alwaysvisible      = true,
      circlealpha        = 1.9,
      circlegrowth       = 0.75,--6
      flashalpha         = 0.01,
      flashsize          = 15,
      ground             = true,
      ttl                = 88,--53
      water              = true,
      color = {
        [1]  = 0.1,
        [2]  = 0.37,
        [3]  = 0.9,
      },
    },
	
	
  },

}

