-- kampfertail

return {
  ["kampfertail"] = {
    bitmapmuzzleflame = {
      air                = true,
      class              = [[CBitmapMuzzleFlame]],
      count              = 1,
      ground             = true,
      underwater         = 1,
      water              = true,
      properties = {
        colormap           = [[0.5 0.7 1 0.01	0.0 0.1 0.2 0.01	0 0 0 0.01]],
        dir                = [[dir]],
        frontoffset        = 0.0,
        fronttexture       = [[dirt]],
        length             = 20,
        sidetexture        = [[shot]],
        size               = 8,
        sizegrowth         = 1.1,
        ttl                = 3,
      },
    },
  },

}

