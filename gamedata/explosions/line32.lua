

    return {
      ["line32"] = {
		  

	
			line = {			
			class = [[CBitmapMuzzleFlame]],
			count = 1,
			air = true,
			ground = true,
			water = true,
			underwater = true,

			properties = {
				colormap = [[ 1 1 1 1]],
				dir = [[dir]],
				frontoffset = 0,
				fronttexture = [[Flake]],
				length = 32,
				sidetexture = [[lightray]],
				size = 1,
				sizegrowth = 0,
				ttl = 100,
			},			
		}				 
    },
    }
