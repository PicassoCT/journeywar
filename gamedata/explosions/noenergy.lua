

return {
	["noenergy"] = {
		
		
		upcross = {			
			air = true,
			class = [[CBitmapMuzzleFlame]],
			count = 1,
			ground = true,
			underwater = false,
			water = true,
			properties = {
				colormap = [[1 0 0 0.5 	0.7 0 0 0.1 	0.5 0 0 0.1 	0.1 0 0 0.01]],
				dir = [[0,1,0]],
				frontoffset = 0.5,
				fronttexture = [[noenergy]],
				length = 25,
				sidetexture = [[]],
				size = 12.5,
				sizegrowth = 0.125,
				ttl = 75,
			},			
		}		
	}
}