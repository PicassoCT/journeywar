---http://springrts.com/wiki/Weapon_Variables#Cannon_.28Plasma.29_Visuals
local weaponName = "shotgunnogamble"
local weaponDef = {
  areaOfEffect       = 8,
  avoidFriendly      = true,
  beamTime           = 0.01,
  beamWeapon         = true,
  burnblow           = true,
  collideFriendly    = false,
  color              = 33,
  coreThickness      = 0.1,
  explosionGenerator = "custom:shotgunImpact",
  impactOnly         = 0,
  impulseFactor      = -1,
  largeBeamLaser     = true,
  lineOfSight        = true,
  name               = "shotgun",
  noSelfDamage       = true,
  projectiles        = 13,
  randomdecay        = "0.3",
  range              = 700,
  reloadtime         = 0.9,
  renderType         = 0,

  soundStart         = "cComEnder/shotgun.ogg",
  soundTrigger       = true,
  sprayAngle         = 2024,

  firestarter = 10,
  texture1           = "shot",
  texture2           = "empty",
  texture3           = "empty",
  texture4           = "empty",
  thickness          = 1,
  tolerance          = 200,
  turret             = false,
  weaponTimer        = 1,
  weaponVelocity     = 700,
  damage = {
    building           = "50",
    commander          = "50",
    default            = 600,
  },
		  }
		  
return lowerkeys({[weaponName] = weaponDef})		  