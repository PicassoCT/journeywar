
local weaponName = "crestrictorthumper"
local weaponDef = {
    name = "cRestricTorThumper",
    weaponType = [[Cannon]],
    damage = {
        default = 3000,
        HeavyArmor = 70,
    },
    explosionScar = false,
    ImpulseBoost = 0.01,
    ImpulseFactor = 0.1,
    areaOfEffect = 450,
    noSelfDamage = true,
    weaponVelocity = 500,
    reloadtime = 0.9,
    range = 210,
    sprayAngle = 0,
    tolerance = 8000,
    lineOfSight = false,
    turret = true,
    craterMult = 0,
    paralyzer = true,
    ParalyzeTime = 25,
    explosionGenerator = "custom:dirt",
}
return lowerkeys({ [weaponName] = weaponDef })
