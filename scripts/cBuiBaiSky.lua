include "createCorpse.lua"
include "lib_OS.lua"
include "lib_UnitScript.lua"
include "lib_Animation.lua"
include "lib_Build.lua"


local SIG_FIRE = 1
local SIG_CRANE = 2
nrOfFires = 13
fireemitters = {}
for i = 1, nrOfFires, 1 do
    fireemitters[i] = {}
    piecename = "fireEmit" .. i
    fireemitters[i] = piece(piecename)
end

ArcoStump = piece "stump"
Blocks = {}

NrOfPoints = 0

buibaicity = piece "buibaicity"
center = piece "center"

local healthMax = 0
local basisCostMetall = 5
local basisCostEnergy = 5
local DoneDamage = 3

local boolwarstarted = false
local boolpeacestarted = true

local basicMetallStorage = 0
local basicEnergyStorage = 0
local NumberofFires = 0
lastAttackingTeamID = nil
previouslyAttackingTeam = nil
boolDamaged = false
teamID = Spring.GetUnitTeam(unitID)


function nothingEverHappend(datTeamID)
    if datTeamID ~= teamID then boolDamaged = true end
    previouslyAttackingTeam = lastAttackingTeamID
    lastAttackingTeamID = datTeamID
    if not previouslyAttackingTeam then previouslyAttackingTeam = lastAttackingTeamID end
end

function SideEffects()
    if not GG.ScumSlumUpgrade then GG.ScumSlumUpgrade = {} end

    IdTable = { [UnitDefNames["cscumslum"].id] = true }
    while true do
        --get builux nearby
        x, y, z = Spring.GetUnitPosition(unitID)
        T = getAllInCircle(x, z, 512, unitID)
        if #T then
            T = getUnitsOfTypeInT(T, IdTable)
            if #T then

                for i = 1, #T do
                    GG.ScumSlumUpgrade[T[i]] = true
                end
            end
        end
        Sleep(1000)
    end
end

function investMent()
    while true do
        if boolDamaged == true then
            StartThread(fireEmit)
            if previouslyAttackingTeam then
                for i = 1, 25, 1 do
                    Spring.AddTeamResource(teamID, "metal", math.ceil(basicMetallStorage / 15))
                    Spring.AddTeamResource(teamID, "energy", math.ceil(basicEnergyStorage / 15))
                    Spring.UseTeamResource(previouslyAttackingTeam, "metal", math.ceil(basicMetallStorage * 2 / 15))
                    Spring.UseTeamResource(previouslyAttackingTeam, "energy", math.ceil(basicEnergyStorage * 2 / 15))
                    Sleep(8000)
                end
            end
            boolDamaged = false
        end
        Sleep(500)
    end
end

function script.HitByWeapon(x, z, weaponDefID, damage)
    boolDelayedStart = true

    return damage
end

function fireEmit()
    while (boolDamaged == true) do
        for i = 1, NrOfPoints, 1 do
            EmitSfx(fireemitters[i], 1025)
            EmitSfx(fireemitters[i], 1026)
            if math.random(0, 1) == 1 then EmitSfx(fireemitters[i], 1027) end
            if math.random(0, 1) == 1 then EmitSfx(fireemitters[i], 1028) end
        end
        Sleep(60)
    end
end


function peaceLoop()
    SetSignalMask(SIG_PEACE)
    while (true) do
        energyRandom = math.ceil(math.random(0, 5))
        metallRandom = math.ceil(math.random(0, 5))


        boolMSuccess = Spring.UseTeamResource(teamID, "metal", metallRandom)
        boolESuccess = Spring.UseTeamResource(teamID, "energy", energyRandom)
        --boolESuccess-->TODO: Cost player basisCostEnergy + energyRandom
        --boolMSuccess<-->TODO: Cost player basisCostMetall + metallRandom
        if boolESuccess ~= nil and boolESuccess == true then
            basicEnergyStorage = basicEnergyStorage + basisCostEnergy + energyRandom
        end
        if boolMSuccess ~= nil and boolMSuccess == true then
            basicMetallStorage = basicMetallStorage + basisCostMetall + metallRandom
        end
        Sleep(1000)
    end
end

function createNewNeonSign(startIndex,endIndex)
	T= Spring.GetUnitPieceInfo(unitID, TableOfPieceGroups["Sign"][1])
	maxSize= 18
	hideT(TableOfPieceGroups["Sign"])
	zero = 0
	exploreTable= makeTable(zero, 8, 8, 0, true)

	arrIndx = {x = 0,y = 0, z= 0}
	boolAccumY = maRa()
	yAccumulated = 0 
	signMax= 2

--	TableOfPieceGroups["Sign"] = shuffleT(TableOfPieceGroups["Sign"])
	intervalStart=math.random(startIndex, startIndex+((endIndex-startIndex)/2))
	intervalEnd = math.random(intervalStart, endIndex)
	for i=intervalStart,intervalEnd do
	name= TableOfPieceGroups["Sign"][i]
	--Show/Hide Piece
	if i > intervalStart and i < intervalEnd then Show(name) end
	
	Move(name,x_axis,arrIndx.x*maxSize,0)
	Move(name,y_axis,arrIndx.y*maxSize,0)
	exploreTable[arrIndx.x][arrIndx.y][arrIndx.z] = exploreTable[arrIndx.x][arrIndx.y][arrIndx.z] + 1
	
	--Turn Piece 
	Spin(name, y_axis, math.rad(yAccumulated), 0)
	Turn(name, y_axis, math.rad(yAccumulated*randSign()), 0)
	if boolAccumY == true then yAccumulated = yAccumulated + 5 end
	
rval= math.random(0,8)*45 + math.random(-15,15)
	Turn(name, z_axis, math.rad(rval),0)
	
	if exploreTable[arrIndx.x ][arrIndx.y][arrIndx.z]  >= signMax then
		if exploreTable[arrIndx.x ][arrIndx.y + 1] and exploreTable[arrIndx.x ][arrIndx.y + 1][arrIndx.z]  < signMax then
			arrIndx.y = arrIndx.y + 1
		else
				-- arrIndx increment
			yOffset= randSign()
			xOffset= randSign()
				if  exploreTable[arrIndx.x + xOffset] and exploreTable[arrIndx.x + xOffset][arrIndx.y+yOffset] and exploreTable[arrIndx.x + xOffset][arrIndx.y+yOffset][arrIndx.z] and signMax > exploreTable[arrIndx.x + xOffset][arrIndx.y+yOffset][arrIndx.z] then
					arrIndx.x, arrIndx.y =arrIndx.x + xOffset,arrIndx.y+yOffset
				else
					while(signMax < exploreTable[arrIndx.x][arrIndx.y][arrIndx.z])do
						arrIndx.y =  ringcrement(arrIndx.y, 8, -8)

						if arrIndx.y== 8 then 
							arrIndx.x = ringcrement(arrIndx.x, 8, -8)
						end
					end
				end	
			end
		end
	end
return intervalStart, intervalEnd
end    

function floatNeonSign(signCenter,startIndex,endIndex )
	rDelay= math.ceil(math.random(100,4000))
	Sleep(rDelay)
	while true do 
		startShowIndex,endShowIndex= createNewNeonSign(startIndex,endIndex)
		xR,zR= math.random(70,80)*randSign(),math.random(70,80)*randSign()
		if maRa()==true then
			Move(signCenter,x_axis,xR,0)
			Move(signCenter,z_axis,math.random(-100,100),0)
		else
			Move(signCenter,z_axis,zR,0)
			Move(signCenter,x_axis,math.random(-100,100),0)
		end

		yR= math.random( 50,200)	
		Move(signCenter, y_axis,yR,0 )
			--Movement and Spin
		WaitForMoves(signCenter)


	Move(signCenter, y_axis, yR + math.random(100,300), 12)
	
	valspin= math.random(-1,1)*15
	Spin(signCenter,y_axis, math.rad(valspin),4)
	WaitForMoves(signCenter)	
	if maRa()==true then
		staticTime=math.random(3000,25000)
		Sleep(staticTime)
		rippleHide(TableOfPieceGroups["Sign"],startShowIndex,endShowIndex)
	end	
	
	for i=startIndex,endIndex do
		reset(TableOfPieceGroups["Sign"][i])
		Hide(TableOfPieceGroups["Sign"][i])
	end

	reset(signCenter)
	Sleep(500)
	end

end

function script.Killed(recentDamage)

    teamID = Spring.GetUnitTeam(unitID)
	createRewardEvent(teamID)

 if lastAttackingTeamID ~= nil then
        boolGotIt = Spring.UseTeamResource(lastAttackingTeamID, "metal", 3900)
        boolGotIt2 = Spring.UseTeamResource(lastAttackingTeamID, "energy", 3900)
        if boolGotIt == false or boolGotIt2 == false then
			createRewardEvent(teamID)
	    end
    end

    xrand = math.random(-2, 2)
    yrand = math.random(-12, 12)
    zrand = math.random(-3, 3)
    Turn(buibaicity, x_axis, math.rad(xrand), 0.02)
    Turn(buibaicity, y_axis, math.rad(yrand), 0.2)
    Turn(buibaicity, z_axis, math.rad(zrand), 0.02)
    Move(buibaicity, y_axis, -180, 11)
    Timer = 0
    while (true == Spring.UnitScript.IsInMove(buibaicity, y_axis)) do
        EmitSfx(center, 1024)
        Sleep(120)
        Timer = Timer + 1
        if Timer == 20 then
            Move(buibaicity, y_axis, -180, 22)
        end
        if Timer == 28 then

            Move(buibaicity, y_axis, -180, 44)
        end
    end
    createCorpseCBuilding(unitID, recentDamage)
    return 1
end

boolDelayedStart = false
function buildIt()

    while boolDelayedStart == false do
        Sleep(100)
    end
    resetT(Blocks)
    hideT(Blocks)
    Hide(ArcoStump)


    if maRa() == true then
        Hide(buibaicity)
        Show(ArcoStump)

        gridTable = {}
        freeSpotList = {}
        for i = -3, 3, 1 do
            gridTable[i] = {}
            for j = -3, 3, 1 do
                gridTable[i][j] = {}

                if math.abs(i) % 2 == 0 and
                        math.abs(j) % 2 == 0 then

                    gridTable[i][j][0] = true
                    freeSpotList[#freeSpotList + 1] = { x = i, y = 0, z = j }
                else
                    gridTable[i][j][0] = false
                end
            end
        end
        buildRandomizedBuilding(Blocks, 160, gridTable, freeSpotList, 22.5)
    else
        hideT(Blocks)
        Hide(ArcoStump)
        Show(buibaicity)
    end
end

TableOfPieceGroups = {}
neonCenter1 = piece"neonCenter1"
neonCenter2 = piece"neonCenter2"
function script.Create()


    TableOfPieceGroups = getPieceTableByNameGroups(false, true)
    Blocks = TableOfPieceGroups["Block"]
    StartThread(buildIt)
    --<buildanimationscript>
    x, y, z = Spring.GetUnitPosition(unitID)
    teamID = Spring.GetUnitTeam(unitID)

    if GG.UnitsToSpawn then
        GG.UnitsToSpawn:PushCreateUnit("cbuildanimation", x, y, z, 0, teamID)
    end

    --</buildanimationscript>
    StartThread(peaceLoop)
    StartThread(investMent)
    StartThread(SideEffects)
    StartThread(floatNeonSign, neonCenter1, 1, 20)
    StartThread(floatNeonSign, neonCenter2, 21,40)
	
	--dramatisPersona3D = initFlyingCars(15)
	--StartThread(littleFlyingCars, dramatisPersona3D)
end

--------BUILDING---------


------------------- globallos -------------------------------