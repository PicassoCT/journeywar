include "lib_OS.lua"
include "lib_UnitScript.lua"
include "lib_Animation.lua"

include "lib_Build.lua"


piecesTable = {}
center = piece "center"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = center
comEndBonker3DS = piece "comEndBonker.3DS"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = comEndBonker3DS
ArmShell1 = piece "ArmShell1"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = ArmShell1
ArmShell2 = piece "ArmShell2"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = ArmShell2
ArmShell3 = piece "ArmShell3"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = ArmShell3

Earth002 = piece "Earth002"


PEarthTable = {}

PEarth01 = piece "PEarth01"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth01
PEarth02 = piece "PEarth02"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth02
PEarth03 = piece "PEarth03"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth03
PEarth04 = piece "PEarth04"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth04
PEarth05 = piece "PEarth05"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth05
PEarth06 = piece "PEarth06"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth06
PEarth07 = piece "PEarth07"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth07
PEarth08 = piece "PEarth08"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth08
PEarth09 = piece "PEarth09"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth09
PEarth010 = piece "PEarth010"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth010
PEarth011 = piece "PEarth011"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth011
PEarth012 = piece "PEarth012"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth012
PEarth013 = piece "PEarth013"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth013
PEarth014 = piece "PEarth014"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth014
PEarth015 = piece "PEarth015"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth015
PEarth016 = piece "PEarth016"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth016
PEarth017 = piece "PEarth017"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth017
PEarth018 = piece "PEarth018"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth018
PEarth019 = piece "PEarth019"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth019
PEarth02 = piece "PEarth02"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth02
PEarth020 = piece "PEarth020"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth020
PEarth021 = piece "PEarth021"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth021
PEarth022 = piece "PEarth022"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth022
PEarth023 = piece "PEarth023"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth023
PEarth024 = piece "PEarth024"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth024
PEarth025 = piece "PEarth025"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth025
PEarth026 = piece "PEarth026"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth026
PEarth027 = piece "PEarth027"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth027
PEarth028 = piece "PEarth028"
PEarthTable[#PEarthTable + 1] = {}
PEarthTable[#PEarthTable] = PEarth028

EarthTable = {}

Earth01 = piece "Earth01"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth01
Earth02 = piece "Earth02"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth02
Earth03 = piece "Earth03"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth03
Earth04 = piece "Earth04"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth04
Earth05 = piece "Earth05"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth05
Earth06 = piece "Earth06"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth06
Earth07 = piece "Earth07"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth07
Earth08 = piece "Earth08"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth08
Earth09 = piece "Earth09"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth09
Earth010 = piece "Earth010"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth010
Earth011 = piece "Earth011"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth011
Earth012 = piece "Earth012"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth012
Earth013 = piece "Earth013"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth013
Earth014 = piece "Earth014"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth014
Earth015 = piece "Earth015"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth015
Earth016 = piece "Earth016"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth016
Earth017 = piece "Earth017"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth017
Earth018 = piece "Earth018"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth018
Earth019 = piece "Earth019"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth019
Earth02 = piece "Earth02"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth02
Earth020 = piece "Earth020"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth020
Earth021 = piece "Earth021"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth021
Earth022 = piece "Earth022"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth022
Earth023 = piece "Earth023"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth023
Earth024 = piece "Earth024"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth024
Earth025 = piece "Earth025"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth025
Earth026 = piece "Earth026"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth026
Earth027 = piece "Earth027"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth027
Earth028 = piece "Earth028"
EarthTable[#EarthTable + 1] = {}
EarthTable[#EarthTable] = Earth028

InnerEleva = piece "InnerEleva"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = InnerEleva
MEcha = piece "MEcha"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = MEcha
Kreis02 = piece "Kreis02"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = Kreis02
Rechteck07 = piece "Rechteck07"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = Rechteck07
bridges1 = piece "bridges1"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = bridges1
bridges2 = piece "bridges2"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = bridges2
bridges3 = piece "bridges3"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = bridges3
bridges4 = piece "bridges4"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = bridges4
bridges5 = piece "bridges5"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = bridges5
bridges6 = piece "bridges6"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = bridges6
crane = piece "crane"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = crane

inAxTable = {}

inAx01 = piece "inAx01"
inAxTable[#inAxTable + 1] = {}
inAxTable[#inAxTable] = inAx01

inAx02 = piece "inAx02"
inAxTable[#inAxTable + 1] = {}
inAxTable[#inAxTable] = inAx02
inAx03 = piece "inAx03"
inAxTable[#inAxTable + 1] = {}
inAxTable[#inAxTable] = inAx03
inAx04 = piece "inAx04"
inAxTable[#inAxTable + 1] = {}
inAxTable[#inAxTable] = inAx04
inAx05 = piece "inAx05"
inAxTable[#inAxTable + 1] = {}
inAxTable[#inAxTable] = inAx05
inAx06 = piece "inAx06"
inAxTable[#inAxTable + 1] = {}
inAxTable[#inAxTable] = inAx06
inAx07 = piece "inAx07"
inAxTable[#inAxTable + 1] = {}
inAxTable[#inAxTable] = inAx07
inAx08 = piece "inAx08"
inAxTable[#inAxTable + 1] = {}
inAxTable[#inAxTable] = inAx08
inAx09 = piece "inAx09"
inAxTable[#inAxTable + 1] = {}
inAxTable[#inAxTable] = inAx09
inAx010 = piece "inAx010"
inAxTable[#inAxTable + 1] = {}
inAxTable[#inAxTable] = inAx010
inAx011 = piece "inAx011"
inAxTable[#inAxTable + 1] = {}
inAxTable[#inAxTable] = inAx011
inAx012 = piece "inAx012"
inAxTable[#inAxTable + 1] = {}
inAxTable[#inAxTable] = inAx012

outAxTable = {}
outAx01 = piece "outAx01"
outAxTable[#outAxTable + 1] = {}
outAxTable[#outAxTable] = outAx01

outAx10 = piece "outAx10"
outAxTable[#outAxTable + 1] = {}
outAxTable[#outAxTable] = outAx10

outAx11 = piece "outAx11"
outAxTable[#outAxTable + 1] = {}
outAxTable[#outAxTable] = outAx11


outAx12 = piece "outAx12"
outAxTable[#outAxTable + 1] = {}
outAxTable[#outAxTable] = outAx12


outAx02 = piece "outAx02"
outAxTable[#outAxTable + 1] = {}
outAxTable[#outAxTable] = outAx02


outAx03 = piece "outAx03"
outAxTable[#outAxTable + 1] = {}
outAxTable[#outAxTable] = outAx03


outAx04 = piece "outAx04"
outAxTable[#outAxTable + 1] = {}
outAxTable[#outAxTable] = outAx04

outAx05 = piece "outAx05"
outAxTable[#outAxTable + 1] = {}
outAxTable[#outAxTable] = outAx05


outAx06 = piece "outAx06"
outAxTable[#outAxTable + 1] = {}
outAxTable[#outAxTable] = outAx06


outAx07 = piece "outAx07"
outAxTable[#outAxTable + 1] = {}
outAxTable[#outAxTable] = outAx07


outAx08 = piece "outAx08"
outAxTable[#outAxTable + 1] = {}
outAxTable[#outAxTable] = outAx08

outAx09 = piece "outAx09"
outAxTable[#outAxTable + 1] = {}
outAxTable[#outAxTable] = outAx09


shieldTable = {}
outShield0 = piece "outShield0"
shieldTable[#shieldTable + 1] = {}
shieldTable[#shieldTable] = outShield0
outShield1 = piece "outShield1"
shieldTable[#shieldTable + 1] = {}
shieldTable[#shieldTable] = outShield1
outShield2 = piece "outShield2"
shieldTable[#shieldTable + 1] = {}
shieldTable[#shieldTable] = outShield2
outShield3 = piece "outShield3"
shieldTable[#shieldTable + 1] = {}
shieldTable[#shieldTable] = outShield3
outShield4 = piece "outShield4"
shieldTable[#shieldTable + 1] = {}
shieldTable[#shieldTable] = outShield4
outShield5 = piece "outShield5"
shieldTable[#shieldTable + 1] = {}
shieldTable[#shieldTable] = outShield5
outShield6 = piece "outShield6"
shieldTable[#shieldTable + 1] = {}
shieldTable[#shieldTable] = outShield6
outShield7 = piece "outShield7"
shieldTable[#shieldTable + 1] = {}
shieldTable[#shieldTable] = outShield7
outShield8 = piece "outShield8"
shieldTable[#shieldTable + 1] = {}
shieldTable[#shieldTable] = outShield8
outShield9 = piece "outShield9"
shieldTable[#shieldTable + 1] = {}
shieldTable[#shieldTable] = outShield9
outShiel10 = piece "outShiel10"
shieldTable[#shieldTable + 1] = {}
shieldTable[#shieldTable] = outShiel10
outShiel11 = piece "outShiel11"
shieldTable[#shieldTable + 1] = {}
shieldTable[#shieldTable] = outShiel11

boolPieceArrived = false
PepTable = {}
for i = 1, 3 do
    PepTable[i] = "pre1" .. i
    PepTable[i + 3] = "pre2" .. i
    PepTable[i] = piece(PepTable[i])
    PepTable[i + 3] = piece(PepTable[i + 3])
end


function showAlarm(index)
    hideT(PepTable)
    Show(PepTable[index])
    Show(PepTable[index + 3])
end

boolPressedButtonTwice = false
teamID = Spring.GetUnitTeam(unitID)

boolFirstActivation = false

function script.Activate()
    if boolFirstActivation == false then boolFirstActivation = true end

    boolPressedButtonTwice = true
    return 1
end

function script.Deactivate()
    return 0
end


function enterTheCommEnder(teamID)

    x, y, z = Spring.GetUnitBasePosition(unitID)

    Spring.CreateUnit("ccomederbunkerdecalfactory", x, y, z, 0, teamID)
    Sleep(10)
    HisID = Spring.CreateUnit("ccomender", x, y, z, 0, teamID)
    Spring.SetUnitBlocking(unitID, false)
    if HisID then
        Spring.SetUnitMoveGoal(HisID, x + 250, y, z)
    end
    HideWrap(MEcha)
    return HisID
end

function showUP()
    Turn(bridges2, y_axis, math.rad(103), 0)
    Turn(bridges3, y_axis, math.rad(-100), 0)
    Turn(bridges4, y_axis, math.rad(60), 0)
    Turn(bridges5, y_axis, math.rad(-145), 0)
    Turn(bridges6, y_axis, math.rad(146), 0)
    t = -47
    for i = 1, #inAxTable, 1 do
        if i < math.ceil(0.5 * #inAxTable) then
            Turn(inAxTable[i], y_axis, math.rad(t - ((i - 1) * 11), 0))
        else
            Turn(inAxTable[i], y_axis, math.rad(47 + ((i - math.ceil(0.5 * #inAxTable)) * 10), 0))
        end
    end

    WaitForMove(center, y_axis)

    Move(Earth002, y_axis, -26.939, 96) --24
    Move(Earth002, z_axis, 61.025, 224) --56
    WaitForMoves(Earth002)
    HideWrap(Earth002)
    Move(ArmShell1, y_axis, -50, 0)



    for i = 1, 29, 1 do

        Sleep(20)
		  
		  ox,oy,oz= Spring.GetUnitPiecePosDir(unitID, EarthTable[i])
	

        StartThread(fallingPhysPieces, 
						EarthTable[i], 
						{x=math.random(-100,100),y = math.random(-10,10), z= math.random(-100,100)},
						{x=0,y=oy,z=0}
						)
    end

    boolPieceArrived = true
    Move(ArmShell1, y_axis, 0, 150)

    while (boolSpawn == false) do

        Sleep(150)
    end
    Sleep(3000)
    Turn(crane, y_axis, math.rad(math.random(-360, 360)), 12)
    budget = 20000
    budget = math.floor(budget / 6) / 1000
    Move(ArmShell3, y_axis, -133, math.abs(-133 / budget))
    WaitForMove(ArmShell3, y_axis)
    Move(ArmShell2, y_axis, -133, math.abs(-133 / budget))
    Turn(crane, y_axis, math.rad(math.random(-360, 360)), 12)
    WaitForMove(ArmShell2, y_axis)
    Move(ArmShell1, y_axis, -213, math.abs(-233 / budget))
    Turn(crane, y_axis, math.rad(math.random(-360, 360)), 12)
    WaitForMove(ArmShell1, y_axis)
    Move(ArmShell3, y_axis, -133 - 60, math.abs(-193 / budget))
    Move(ArmShell2, y_axis, -133 - 60, math.abs(-193 / budget))
    Turn(crane, y_axis, math.rad(math.random(-360, 360)), 12)
    WaitForMove(ArmShell3, y_axis)
    Turn(crane, y_axis, math.rad(math.random(-360, 360)), 12)
    WaitForMove(ArmShell2, y_axis)
    for i = 1, #inAxTable, 1 do
        Turn(inAxTable[i], x_axis, math.rad(77), 12 / (budget * i))
    end

    for i = 1, #outAxTable, 1 do
        Turn(outAxTable[i], x_axis, math.rad(-99), math.abs(-20 / (budget * i)))
    end

    for i = 1, #shieldTable, 1 do
        Turn(shieldTable[i], x_axis, math.rad(120), math.abs(20 / (budget * i)))
    end
    Turn(bridges2, y_axis, math.rad(0), 2.6)
    Turn(bridges3, y_axis, math.rad(0), 2.6)
    Turn(bridges4, y_axis, math.rad(0), 2.6)
    Turn(bridges5, y_axis, math.rad(0), 2.6)
    Turn(bridges6, y_axis, math.rad(0), 2.6)

    Turn(crane, y_axis, 0, 1.2)
    Move(Kreis02, y_axis, -72, 15)
end

function inYourOwnTime()
    while (boolFirstActivation == false) do
        Sleep(50)
    end
    StartThread(spawnCommander)
    Move(center, y_axis, 0, 60)
    StartThread(showUP)
end

function script.Create()
    showAlarm(1)
    resetT(EarthTable)
    Move(center, y_axis, -300, 0)
    StartThread(inYourOwnTime)
end

boolSpawn = false
function spawnCommander()
    if GG.ComEnders == nil then GG.ComEnders = {} end

    if GG.ComEnders[teamID] == nil then
        Sleep(4200)

        --play Warning Message Here: "Warning! You requested direct insertion into the Battlefield. By Universal Union Law the Buttonpressing Individual hereby, declares The U^2 responsibilitys nil and void- should he find irreversible neural death on the Battlefield! Button Up here."
        Spring.PlaySoundFile("sounds/citadell/bushthebutton.wav")
        Sleep(20000)
        boolPressedButtonTwice = false
        showAlarm(2)
        while boolPressedButtonTwice == false do
            showAlarm(2)
            Sleep(350)
            showAlarm(1)
            Sleep(350)
        end
        showAlarm(3)
        -- Screen goes Black and then comes back
        --play Warning Message: "Binding Agreement is processed. Comander teleported. Comander Capsule inserted. Lock Sealed. Atmospheric Preasure in HangarBay. Please keep limbs inside the capsule at all times. Outside Temperature is 42 � Degrees. Good Luck, Comander."
        Spring.PlaySoundFile("sounds/citadell/comEnderDeploy.wav")
        --check ComEnderTable for not allready spawned
        boolSpawn = true
        Sleep(22000)
        lteamID = Spring.GetUnitTeam(unitID)
        --spin UP Sensor



        if GG.ComEnders == nil then GG.ComEnders = {} end
        if GG.ComEnders[teamID] == nil then
            idEal = enterTheCommEnder(lteamID)
            GG.ComEnders[teamID] = idEal
            Sleep(5000)
            for i = 1, #inAxTable, 1 do
                Turn(inAxTable[i], x_axis, math.rad(0), 1.2 / i)
            end

            for i = 1, #outAxTable, 1 do
                Turn(outAxTable[i], x_axis, math.rad(0), 1.2 / i)
            end

            for i = 1, #shieldTable, 1 do
                Turn(shieldTable[i], x_axis, math.rad(0), 1.2 / i)
            end
            showAlarm(2)
            WMove(center, y_axis, -50, 5)
            showAlarm(1)
            Move(center, y_axis, -150, 5)
        end
    end

    if math.random(0, 1) == 1 then
        Sleep(6 * 60 * 10000)
        Spring.PlaySoundFile("sounds/citadell/centpropagendspeech.ogg", 0.9)
    end
end

function script.Killed()
    return 0
end