include "lib_jw.lua"
include "lib_UnitScript.lua"
include "lib_Animation.lua"
include "createCorpse.lua"

globalCityWideAlarm = 0
local condepotre = piece "condepotre"
local condepotbu = piece "condepotbu"
local condepotb0 = piece "condepotb0"
local condboltlo = piece "condboltlo"

local condbolt1 = piece "condbolt1"
local condbolt2 = piece "condbolt2"
local condbolt3 = piece "condbolt3"
local condbolt4 = piece "condbolt4"

local cdturningpoint = piece "cdturningpoint"

buildspottrain = piece "buildspottrain"

buildspotplane1 = piece "buildspotplane"
buildspotplane2 = piece "buildspotplane2"
buildspotplane3 = piece "buildspotplane3"
buildspotplane4 = piece "buildspotplane4"

buildspottruck1 = piece "buildspottruck"
buildspottruck2 = piece "buildspottruck2"
buildspottruck3 = piece "buildspottruck3"
buildspottruck4 = piece "buildspottruck4"

local condepotli = piece "condepotli"
local condepotl2 = piece "condepotl2"
local condepotl0 = piece "condepotl0"
local condepotl1 = piece "condepotl1"

local boolRetracted = true
boolBuildSpotFixxed = false
local unitDefID = 0
local boolIsBuilding = false
currentBuildSpot = piece "buildspottrain"
local cdcrane = piece "cdcrane"
local SIG_BUILD = 2
local buildID = nil
local SIG_InActivate = 4
local SIG_OPEN = 8
local SIG_CLOSE = 32
local SIG_IDLE = 16
local SIG_RESET = 64

local boolStateSet = true
local boolStable = true
boolBuildEnder = true

teamID = Spring.GetUnitTeam(unitID)
TableOfPieceGroups = {}

function script.Create()
    StartThread(lightThread)
    --<buildanimationscript>
    x, y, z = Spring.GetUnitPosition(unitID)
    TableOfPieceGroups = getPieceTableByNameGroups(false, true)
    GG.UnitsToSpawn:PushCreateUnit("cbuildanimation", x, y, z, 0, teamID)
    hideT(TableOfPieceGroups["cdcontain"])
    --</buildanimationscript>

    Hide(condepotli)
    Hide(condepotl2)
    Hide(condepotl0)
    Hide(condepotl1)
    --Spring.Echo("GOTHISFAR")
    StartThread(open)
    StartThread(unitsIsBuilding)
    StartThread(alarmCheck)
    StartThread(containerAnimation)
end

rightcontainer = piece("rightcontainer")
leftcontainer = piece("leftcontainer")

function containerAnimation()
    while true do
        buildID = Spring.GetUnitIsBuilding(unitID)
        if buildID then
            WMove(rightcontainer, y_axis, -6, 0)
            WMove(leftcontainer, y_axis, -6, 0)
            showT(TableOfPieceGroups["cdcontain"])
            Move(rightcontainer, y_axis, 0, 3)
            Move(leftcontainer, y_axis, 0, 3)
            for i = 15, 18 do
                Sleep(250)
                Hide(TableOfPieceGroups["cdcontain"][i])
            end
            for i = 5, 8 do
                Sleep(250)
                Hide(TableOfPieceGroups["cdcontain"][i])
            end
            WMove(rightcontainer, y_axis, 0, 3)
            WMove(leftcontainer, y_axis, 0, 3)
        else
            hideT(TableOfPieceGroups["cdcontain"])
        end
        Sleep(350)
    end
end

function script.QueryBuildInfo()
    assert(type(currentBuildSpot) == "number", "condepot::" .. type(currentBuildSpot))
    return currentBuildSpot
end

Spring.SetUnitNanoPieces(unitID, { cdcrane })

function building()
    boolIsBuilding = true
    --SetSignalMask(SIG_BUILD)
    local spthemLights = themLights
    while (boolIsBuilding == true) do
        Sleep(150)
        rand = math.random(0, 1)
        themLights()
        if rand == 1 then
            randspeed = math.random(2.5, 3.5)
            Move(cdcrane, x_axis, 50, randspeed)
            WaitForMove(cdcrane, x_axis)
        end
        spthemLights()
        if rand == 0 then
            randspeed = math.random(2.5, 3.5)
            Move(cdcrane, x_axis, -50, randspeed)
            WaitForMove(cdcrane, x_axis)
        end

        spthemLights()
    end
    boolBuildSpotFixxed = false
end

function unitsIsBuilding()
    --Spring.Echo("Script gets Executed")
    local conAirDefID = UnitDefNames["conair"].id
    local conTrainDefID = UnitDefNames["contrain"].id
    local conTruckDefID = UnitDefNames["contruck"].id
    local spGetUnitIsBuilding = Spring.GetUnitIsBuilding
    local spGetUnitDefID = Spring.GetUnitDefID

    while (true) do
        Sleep(100)
        if boolIsBuilding == false then
            boolBuildSpotFixxed = false
        end

        if boolIsBuilding == true and boolBuildSpotFixxed == false then
            Sleep(10)
            buildID = spGetUnitIsBuilding(unitID)
            Sleep(10)
            while (buildID == nil) do
                Sleep(100)
                buildID = spGetUnitIsBuilding(unitID)
            end

            unitDefID = spGetUnitDefID(buildID)

            while (unitDefID ~= nil and buildID ~= nil and boolBuildSpotFixxed == false) do
                if unitDefID == conTrainDefID then
                    boolBuildSpotFixxed = true
                    currentBuildSpot = buildspottrain

                elseif unitDefID == conAirDefID then
                    vierGewinnt = math.random(0, 3)
                    if vierGewinnt == 0 then
                        boolBuildSpotFixxed = true
                        currentBuildSpot = buildspotplane1
                    elseif vierGewinnt == 1 then
                        boolBuildSpotFixxed = true
                        currentBuildSpot = buildspotplane2
                    elseif vierGewinnt == 2 then
                        boolBuildSpotFixxed = true
                        currentBuildSpot = buildspotplane3
                    elseif vierGewinnt == 3 then
                        boolBuildSpotFixxed = true
                        currentBuildSpot = buildspotplane4
                    end
                elseif unitDefID == conTruckDefID then
                    vierGewinnt = math.random(0, 3)


                    if vierGewinnt == 0 then
                        boolBuildSpotFixxed = true
                        currentBuildSpot = buildspottruck1
                    elseif vierGewinnt == 1 then
                        boolBuildSpotFixxed = true
                        currentBuildSpot = buildspottruck2
                    elseif vierGewinnt == 2 then
                        boolBuildSpotFixxed = true
                        currentBuildSpot = buildspottruck3
                    elseif vierGewinnt == 3 then
                        boolBuildSpotFixxed = true
                        currentBuildSpot = buildspottruck4
                    end

                else
                    boolBuildSpotFixxed = false
                    Sleep(50)
                    --Spring.Echo("BuildSpotUnfixxed")
                end
            end
            Sleep(300)
        end
    end
end

function script.Killed(recentDamage, _)
    WMove(cdcrane, x_axis, 24, 38)

    process(TableOfPieceGroups["cdcontain"],
        function(id)
            Hide(id)
            Explode(id, SFX.NONE + SFX.FALL)
            randi = math.random(1, 10)
            Sleep(randi)
        end)

    for i = 1, 5 do
        WMove(condepotre, y_axis, -100 / 5 * i, 90)
        Explode(condepotre, SFX.SHATTER)
    end

    Explode(condepotbu, SFX.FALL + SFX.FIRE)
    Explode(condepotb0, SFX.FALL + SFX.FIRE)
    --
    Explode(buildspottrain, SFX.FIRE + SFX.FALL + SFX.EXPLODE_ON_HIT)

    Explode(buildspotplane2, SFX.FIRE + SFX.FALL + SFX.EXPLODE_ON_HIT)
    Explode(buildspotplane3, SFX.FIRE + SFX.FALL + SFX.EXPLODE_ON_HIT)
    Explode(buildspotplane4, SFX.FIRE + SFX.FALL + SFX.EXPLODE_ON_HIT)
    Explode(buildspottruck1, SFX.FIRE + SFX.FALL + SFX.EXPLODE_ON_HIT)
    Explode(buildspottruck2, SFX.FIRE + SFX.FALL + SFX.EXPLODE_ON_HIT)
    Explode(buildspottruck3, SFX.FIRE + SFX.FALL + SFX.EXPLODE_ON_HIT)
    Explode(buildspottruck4, SFX.FIRE + SFX.FALL + SFX.EXPLODE_ON_HIT)
    Explode(condepotre, SFX.SHATTER)
    return 0
end

function open()
    boolRetracted = false
    Spring.PlaySoundFile("sounds/cbuil/cbuilrise.wav")
    Signal(SIG_CLOSE)
    SetSignalMask(SIG_OPEN)

    Spin(condboltlo, y_axis, 0, 0.7)

    Sleep(1100)
    Move(condboltlo, x_axis, 0, 0.3)
    WaitForMove(condboltlo, y_axis)
    Hide(condboltlo)
    Turn(condepotbu, z_axis, math.rad(0), 0.35)
    Turn(condepotb0, z_axis, math.rad(0), 0.35)
    Move(condepotbu, y_axis, 0, 5)
    Move(condepotb0, y_axis, 0, 5)
    -- i actually imagined the controlltower to be the key_lock of that blasted door, but had enough trouble as it is...
    --	still would be fucking awesome
    Sleep(1250)
    Move(condepotre, y_axis, 0, 6)
    WaitForMove(condepotre, y_axis)
    WaitForMove(condepotb0, y_axis)
    WaitForMove(condepotbu, y_axis)

    Turn(condepotbu, x_axis, math.rad(0), 12)
    Turn(condepotbu, y_axis, math.rad(0), 12)
    Turn(condepotbu, z_axis, math.rad(0), 12)

    Turn(condepotb0, x_axis, math.rad(0), 12)
    Turn(condepotb0, y_axis, math.rad(0), 12)
    Turn(condepotb0, z_axis, math.rad(0), 12)
    Move(condboltlo, x_axis, 0, 5)
    Move(condboltlo, y_axis, 0, 5)
    Move(condboltlo, z_axis, 0, 5)
    Turn(condboltlo, x_axis, math.rad(0), 12)
    Turn(condboltlo, y_axis, math.rad(0), 12)
    Turn(condboltlo, z_axis, math.rad(0), 12)

    while (true) do
        Sleep(400)
    end
end

function close()
    boolRetracted = true

    Spring.PlaySoundFile("sounds/cbuil/cbuilretr.wav")
    Signal(SIG_OPEN)
    --Hide(condboltlo)
    SetSignalMask(SIG_CLOSE)
    Move(condboltlo, x_axis, 10, 5)
    WaitForMove(condboltlo, x_axis)
    Move(condepotre, y_axis, -222, 6)
    WaitForMove(condepotre, y_axis)
    Move(condepotbu, y_axis, 45, 5)
    Move(condepotb0, y_axis, 45, 5)
    Move(condepotbu, x_axis, -11, 5)
    Move(condepotb0, x_axis, 11, 5)
    Turn(condepotbu, z_axis, math.rad(45), 0.05)
    Turn(condepotb0, z_axis, math.rad(-45), 0.05)
    Move(condepotbu, y_axis, 93, 5)
    Move(condepotb0, y_axis, 93, 5)
    Sleep(189)

    Turn(condepotbu, z_axis, math.rad(90), 0.05)
    Turn(condepotb0, z_axis, math.rad(-90), 0.05)
    Move(condbolt1, y_axis, -14, 1)
    Move(condbolt2, y_axis, -14, 1)
    Move(condbolt3, y_axis, -14, 1)
    Move(condbolt4, y_axis, -14, 1)

    Sleep(1200)
    Move(condepotbu, x_axis, 0, 0.5)
    Move(condepotb0, x_axis, 0, 0.5)
    Spring.PlaySoundFile("sounds/cComon/cBunkerShut.wav")
    WaitForTurn(condepotbu, z_axis)
    WaitForTurn(condepotb0, z_axis)

    WaitForMove(condbolt4, y_axis)
    WaitForMove(condbolt3, y_axis)
    WaitForMove(condbolt2, y_axis)
    WaitForMove(condbolt1, y_axis)
    --- -condepotbolts

    Sleep(1200)

    Show(condboltlo)
    Move(condboltlo, x_axis, 9, 1)

    WaitForMove(condboltlo, x_axis)
    Turn(condboltlo, x_axis, math.rad(180), 0.85)
    WaitForTurn(condboltlo, x_axis)
    Turn(condboltlo, x_axis, math.rad(360), 0.2)
    WaitForTurn(condboltlo, x_axis)
    Move(condboltlo, x_axis, 12, 4)
    Move(condepotb0, x_axis, 0.5, 2)
    Move(condepotbu, x_axis, -0.5, 2)
    Sleep(2500)
    Spin(condboltlo, x_axis, math.rad(20), 0.7)
    Move(condboltlo, x_axis, 4, 0.5)
    WaitForMove(condboltlo, x_axis)
    StopSpin(condboltlo, x_axis)
    --Waits

    Move(condboltlo, x_axis, 0, 2)
    Spin(condboltlo, x_axis, math.rad(40), 0.7)
    Sleep(700)
    StopSpin(condboltlo, x_axis)
    Turn(condboltlo, x_axis, math.rad(0), 0.7)
    Move(condbolt1, y_axis, 0, 2)
    Move(condbolt2, y_axis, 0, 2)
    Move(condbolt3, y_axis, 0, 2)
    Move(condbolt4, y_axis, 0, 2)
    --waitforTurn
    Move(condboltlo, x_axis, 10, 2)

    Signal(SIG_IDLE)

    while (true) do
        Sleep(400)
        --SetUnitValue(COB.INBUILDSTANCE, 0)
    end
end

function themLights()
    Show(condepotli)
    Sleep(300)
    Show(condepotl2)
    Hide(condepotli)
    Sleep(300)
    Hide(condepotl2)
    Show(condepotl1)
    Sleep(300)
    Show(condepotl0)
    Hide(condepotl1)
    Sleep(300)
    Hide(condepotl0)
    Sleep(550)
end

--In Spring every Thread ends, when its spawning parent ends, thus this does not need to be handled
function lightThread()
    while (true) do
        themLights()
    end
end

local function idle()
    SetSignalMask(SIG_IDLE)

    while (true) do

        randMove = math.random(0, 51)
        randSpeed = math.random(0.4, 4)
        Move(cdcrane, x_axis, randMove, randSpeed)
        WaitForMove(cdcrane, x_axis)
        randSleep = math.random(512, 1024)
        Sleep(randSleep)
        -- Light chain as function
        randMove = math.random(-48, -25)
        Move(cdcrane, x_axis, randMove, randSpeed)
        WaitForMove(cdcrane, x_axis)
    end
end

function script.Activate()
    --Spring.Echo("We shall see about that")
    if GG.Alarm == nil then
        GG.Alarm = {}
        GG.Alarm[teamID] = {}
    end
    GG.Alarm[teamID] = false


    SetUnitValue(COB.YARD_OPEN, 1)
    SetUnitValue(COB.INBUILDSTANCE, 1)
    SetUnitValue(COB.BUGGER_OFF, 1)

    return 1
end

function script.Deactivate()
    if GG.Alarm == nil then
        GG.Alarm = {}
        GG.Alarm[teamID] = {}
    end
    GG.Alarm[teamID] = true

    SetUnitValue(COB.YARD_OPEN, 0)
    SetUnitValue(COB.INBUILDSTANCE, 0)
    SetUnitValue(COB.BUGGER_OFF, 0)

    return 0
end

function script.StopBuilding()
    boolIsBuilding = false
end

function script.StartBuilding(heading, pitch)
    --Spring.Echo("First to die of thirst?")
    Signal(SIG_RESET)
    StartThread(building)

    boolIsBuilding = true
end

statusOfOld = nil

function alarmCheck()
    if GG.Alarm == nil then
        GG.Alarm = {}
        GG.Alarm[teamID] = {}
        GG.Alarm[teamID] = false
    end

    if GG.Alarm[teamID] == true then
        statusOfOld = true
    else
        statusOfOld = false
    end

    while (true) do
        if GG.Alarm[teamID] == true and GG.Alarm[teamID] ~= statusOfOld then
            statusOfOld = GG.Alarm[teamID]
            --case Alarm and building is still open
            boolClosingTimes = true
            Signal(SIG_OPEN)
            StartThread(close)
        end

        if GG.Alarm[teamID] == false and GG.Alarm[teamID] ~= statusOfOld then
            --case no Alarm and building is still retracted
            statusOfOld = GG.Alarm[teamID]
            Signal(SIG_CLOSE)
            StartThread(open)
        end

        Sleep(4095)
    end
end