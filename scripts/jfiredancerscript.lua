include "createCorpse.lua"
include "lib_OS.lua"
include "lib_UnitScript.lua"
include "lib_Animation.lua"
include "lib_Build.lua"





--Define the pieces of the weapon
local Head = piece "Head"
local ArmR = piece "ArmR"
local ArmL = piece "ArmL"
local staff = piece "staff"
local flare02 = piece "staff"
local center = piece "center"
local LegR = piece "LegR"
local LLegR = piece "LLegR"
local LegL = piece "LegL"
local LLegL = piece "LLegL"


--define other pieces
local body = piece "body"
local SIG_WALK = 1 --signal for the walk animation thread
local SIG_AIM = 2 --signal for the weapon aiming thread
local SIG_IDLE = 4
local SIG_AIM2 = 8

SalvoSize= 8



--eggspawn --tigLil and SkinFantry
boolIcanFly = false
airUnitTypes= getAirUnitTypeTable(UnitDefNames)
function wizzardOfOS()
	delayTillComplete(unitID)
	
    while true do
        if boolIcanFly == true then
            --turn this whole thing into a groundtorpedo

            local x, y, z = Spring.GetUnitPosition(unitID)
            local teamID = Spring.GetUnitTeam(unitID)
            Spring.CreateUnit("jfiredancedecal", x, y, z, math.ceil(math.random(0, 3)), teamID)
			
			T= getAllInSphere(x,y,z,1000)
			process(T,
					function(id)
						if airUnitTypes[Spring.GetUnitDefID(id)] then
						local FireWeaponDef = {
								pos = { x, y + 20, z },
								tracking = id,
								speed = { 0, 1, 0 },
								owner = unitID,
								team = teamid,
								spread = { math.random(-5, 5), math.random(-5, 5), math.random(-5, 5) },
								ttl = 420,
								error = { 0, 0, 0 },
								maxRange = 1000,
								gravity = Game.gravity,
								startAlpha = 1,
								endAlpha = 1,
								model = "jfireProj.s3o",
								cegTag = "firedancerprojsfx"
							}
							jfiredancerprojDefID= WeaponDefNames["jfiredancerproj"].id
							projectileID = Spring.SpawnProjectile(ChainLightningDefID, FireWeaponDef)
						end
					end
					)
				Spring.DestroyUnit(unitID,true,false)
				
			end
        Sleep(100)
    end
end

myDefID=Spring.GetUnitDefID(unitID)
function playSound()
    if not GG.FireDance then
        GG.FireDance = {}
        GG.FireDance[1] = math.floor(math.random(1, 6))
    end
	
    String = "sounds/jfiredancer/jfiredance"
    String2 = ".ogg"
	 GG.FireDance[1]= (GG.FireDance[1]%6)+1

	Res = String .. (GG.FireDance[1]) .. String2	
	PlaySoundByUnitDefID(myDefID, bgdefID, Res, 1, GG.FireDance[2] , 1, 0)
end


function script.Activate()
    boolIcanFly = true
    --	StartThread(alarmCheck)
    return 1
end

function script.Deactivate()

    --StartThread(alarmCheck)

    return 0
end

TableOfPieceGroups = {}
function script.Create()
	TableOfPieceGroups = getPieceTableByNameGroups(false, true)

	Hide(staff)
    StartThread(wizzardOfOS)

    StartThread(downHillGlideDetector)
    StartThread(reloadCycle)
end

idleFunctions= {
[1]=function()
   Turn(center, x_axis, math.rad(69), 4)
            Turn(ArmL, x_axis, math.rad(-90), 7)
            Turn(ArmR, x_axis, math.rad(-90), 7)
            ringAlingADingDong = math.random(2, 26)
            WaitForTurn(center, x_axis)
            for i = 0, ringAlingADingDong, 1 do
                Turn(center, x_axis, math.rad(81), 2)
                Turn(ArmL, x_axis, math.rad(-157), 12)
                Turn(ArmR, x_axis, math.rad(-157), 12)
                Turn(staff, x_axis, math.rad(75), 12)
                WaitForTurn(center, x_axis)
                tRand = math.random(20, 100)
                Sleep(tRand)
                Turn(center, x_axis, math.rad(62), 1)
                Turn(ArmL, x_axis, math.rad(-84), 11)
                Turn(ArmR, x_axis, math.rad(-84), 11)
                Turn(staff, x_axis, math.rad(21), 11)
                WaitForTurn(center, x_axis)
                Sleep(120)
            end
end,
[2]=function()
 Turn(center, x_axis, math.rad(85), 6)
            Turn(ArmL, x_axis, math.rad(-170), 7)
            Turn(ArmR, x_axis, math.rad(-170), 7)


            Turn(ArmR, x_axis, math.rad(-175), 7)
end,
[3]=function()
			Move(center, y_axis, -7, 2)
            tP(Head, -24, 0, 0, 1)
            tP(ArmR, -91, -44, 0, 1)
            tP(ArmL, -91, 38, 0, 1)
            tP(LegR, -43, 32, 0, 4)
            tP(LLegR, 135, 0, 0, 4)
            tP(LegL, -43, -32, 0, 4)
            tP(LLegL, 135, 0, 0, 4)
            if math.random(0, 1) == 1 then
                for i = 1, 12 do
                    rand = math.random(-35, 35)
                    tP(ArmR, -91 + rand, -44, 0, 1)
                    tP(ArmL, -91 + rand, 38, 0, 1)
                    tP(staff, -1 * rand, 0, 0, 1)
                    WaitForTurns(ArmR, ArmL, staff)
                    Sleep(2000)
                end
            else
                Sleep(12000)
            end
end,
[4]=function()

   tP(ArmL, 12, 0, -31, 5)
            tP(ArmR, -172, 0, 30, 5)
            tP(LLegR, 152, 0, 0, 5)
            tP(LegR, -77, 0, 0, 5)
            tP(LegL, 0, 0, 13, 5)
            tP(LLegL, 0, 0, -13, 5)
            tP(Head, -20, 0, 0, 5)
            Spin(staff, y_axis, math.rad(400), 0)
            WaitForTurns(ArmL, ArmR, LLegR, LegR, LLegL, LegL, Head)
            tP(ArmL, -90, 0, -31, 5, true)
            tP(ArmL, -184, 0, 42, 5, true)
            tP(ArmR, -172, 0, -30, 5)

            WaitForTurns(ArmL, ArmR, LLegR, LegR, LLegL, LegL, Head)
            for i = 1, 9 do
                Spin(staff, y_axis, math.rad(400 * -1 ^ i), 0)
                if i % 2 == 0 then stompLeg(LegL, LLegL) else stompLeg(LegR, LLegR) end
                randVal = math.random(-32, 32)
                Turn(staff, x_axis, math.rad(randVal), 11)
                Sleep(1000)
            end
            StopSpin(staff, y_axis, 1)
            reset(staff, 1)
end,
[5]=function()
    Turn(ArmR, x_axis, math.rad(-90), 7)
            Turn(ArmL, x_axis, math.rad(-90), 7)
            WaitForTurn(ArmR, x_axis)
            WaitForTurn(ArmL, x_axis)

            sportIsMord = math.random(0, 128)
            for it = 0, sportIsMord, 1 do
                Move(body, y_axis, -4, 12)
                Turn(LegR, x_axis, math.rad(-42), 3)
                Turn(LegL, x_axis, math.rad(-42), 3)
                Turn(LLegR, x_axis, math.rad(121), 10)
                Turn(LLegL, x_axis, math.rad(121), 10)
                Turn(ArmR, x_axis, math.rad(-80), 2)
                Turn(ArmL, x_axis, math.rad(-82), 2)

                WaitForTurn(ArmR, x_axis)
                WaitForTurn(ArmL, x_axis)
                WaitForTurn(LegR, x_axis)
                WaitForTurn(LegL, x_axis)
                WaitForTurn(LLegR, x_axis)
                WaitForTurn(LLegL, x_axis)

                WaitForMove(body, y_axis)

                fixFertig = math.random(20, 120)
                Sleep(fixFertig)
                Turn(ArmR, x_axis, math.rad(-90), 3)
                Turn(ArmL, x_axis, math.rad(-90), 3)
                Turn(LegR, x_axis, math.rad(0), 3)
                Turn(LegL, x_axis, math.rad(0), 3)
                Turn(LLegR, x_axis, math.rad(0), 5)
                Turn(LLegL, x_axis, math.rad(0), 5)
                Move(body, y_axis, 0, 16)
                WaitForTurn(ArmR, x_axis)
                WaitForTurn(ArmL, x_axis)
                WaitForTurn(LegR, x_axis)
                WaitForTurn(LegL, x_axis)
                WaitForTurn(LLegR, x_axis)
                WaitForTurn(LLegL, x_axis)


                WaitForMove(body, y_axis)
                fixFertig = math.random(80, 620)
                Sleep(fixFertig)
            end
            Turn(LegR, x_axis, math.rad(70), 5)
            Turn(LLegR, x_axis, math.rad(118), 5)
            Turn(ArmR, x_axis, math.rad(-3), 3)
            Turn(ArmR, y_axis, math.rad(-88), 3)
            Turn(ArmR, z_axis, math.rad(77), 3)
            Turn(ArmL, z_axis, math.rad(-77), 3)
            Sleep(14000)
end,
[6]=function()
	rArm= math.random(-2,0)
	 Turn(ArmR, y_axis, math.rad(90), 7)
	 Turn(ArmR, z_axis, math.rad(rArm*90), 7)

	 lArm= math.random(-0,2)
	 Turn(ArmL, y_axis, math.rad(90), 7)
	 Turn(ArmL, z_axis, math.rad(lArm*90), 7)
	--Katamotion
	rBody = math.random(-1,1)
	Turn(body,y_axis,math.rad(90*rBody),12)
	tP(LegR,15,90*rBody*-1,0,12)
	tP(LLegR,-15,0,0,12)
	tP(LegL,-15,90*rBody*-1,0,12)
	tP(LLegL,15,0,0,12)
	
end

}

local function idle()

    sleeper = math.random(1024, 8192)

    SetSignalMask(SIG_IDLE)
    while (true) do
        Move(center, x_axis, 0, 12)
        Move(center, y_axis, 0, 12)
        Move(center, z_axis, 0, 12)
        Move(body, x_axis, 0, 12)
        Move(body, y_axis, 0, 12)
        Move(body, z_axis, 0, 12)
        Turn(center, x_axis, math.rad(0), 15)
        Turn(center, y_axis, math.rad(0), 15)
        Turn(center, z_axis, math.rad(0), 15)
        Turn(body, x_axis, math.rad(0), 15)
        Turn(body, y_axis, math.rad(0), 15)
        Turn(body, z_axis, math.rad(0), 15)
        Turn(LegL, x_axis, math.rad(0), 15)
        Turn(LegR, x_axis, math.rad(0), 15)
        Turn(LLegR, x_axis, math.rad(0), 32)
        Turn(LLegL, x_axis, math.rad(0), 32)
        Sleep(sleeper)

		idleFunctions[math.random(0,#idleFunctions)]()
		Sleep(5000)
		playSound()
    end
end

function stompLeg(upLeg, dLeg)
    tP(dLeg, 152, 0, 0, 5)
    tP(upLeg, -77, 0, 0, 5)
    WaitForTurns(upLeg, dLeg)
    tP(dLeg, 0, 0, 0, 5)
    tP(upLeg, 0, 0, 0, 5)
    WaitForTurns(upLeg, dLeg)
end

function walk()

    Signal(SIG_WALK)
    SetSignalMask(SIG_WALK)
    --Turn(center, y_axis, math.rad(0), 34)
    Turn(body, x_axis, math.rad(0), 34)
    WaitForTurn(body, x_axis)
    leg_movespeed = 3
    times = 0
    step = math.pi / 4
    while (true) do
        leg_movespeed = math.min(3, leg_movespeed + 0.1)

        sway = math.sin(times)
        tP(Head, -15, 0, 0, leg_movespeed)
        tP(center, 15, math.pi * sway, 0, leg_movespeed)
        tP(LegR, -42, 0, 0, leg_movespeed)
        tP(LLegR, 20, 0, 0, leg_movespeed)
        tP(LegL, 11, 0, 0, leg_movespeed)
        tP(LLegL, 15, 0, 0, leg_movespeed)
		if boolAiming== false then
        tP(ArmL, -26, 0, 0, leg_movespeed)
        tP(ArmR, 37, 0, 0, leg_movespeed)
		end
		
        WaitForTurns(LLegR, LegR, LLegL, LegL, Head, center, ArmL, ArmR)
        times = times + step
        tP(LegL, -42, 0, 0, leg_movespeed)
        tP(LLegL, 20, 0, 0, leg_movespeed)
        tP(LegR, 11, 0, 0, leg_movespeed)
        tP(LLegR, 15, 0, 0, leg_movespeed)
		if boolAiming== false then
        tP(ArmL, 37, 0, 0, leg_movespeed)
        tP(ArmR, -16, 0, 0, leg_movespeed)
		end
		
        --left leg up, right leg down
        tP(Head, -21, 0, 0, leg_movespeed)
        tP(center, 10, -math.pi * sway, 0, leg_movespeed)
        WaitForTurns(LLegR, LegR, LLegL, LegL, Head, center, ArmL, ArmR)
        times = times + step
    end
end

--- -aimining & fire weapon
function script.AimFromWeapon1()
    return ArmR
end

function script.QueryWeapon1()
    return Head
end

function reloadCycle()
while true do
if boolFiring == true then
Sleep(4000)
boolFiring=false
end
Sleep(100)
end
end


aimTime=Spring.GetGameFrame()
boolAiming=false
function aimAnimation(Heading,pitch)
		Signal(SIG_IDLE)
		boolAiming=true
		x,y,z=Spring.GetUnitPosition(unitID)
		factor= clamp(0, 1- (((Spring.GetGameFrame() - aimTime)))/(30*5),1)
		tP(ArmL,-175,0,-90*factor, 	5)
		tP(Head,-21,0,0, 	5)
		tP(ArmR,-175,0,90*factor, 	5)

		if maRa()==true then
			spawnCegAtPiece(unitID,body,"firecolumn",1)		
		else
			spawnCegAtPiece(unitID,Head,"firecolumn",1)		
		end
		

        
		if Heading then 
			Turn(center, y_axis, Heading, 15)
		end
        WaitForTurns(ArmR,ArmL,center)
		if factor <= 0 then
		--Spring.SpawnCEG( "nanofirestart",x,y+50,z, math.random(-10, 10) / 10, math.random(0, 10) / 10, math.random(-10, 10) / 10)
        Spring.CreateUnit("jfiredancedecal", x, y, z, math.ceil(math.random(0, 3)), teamID)
		aimTime=Spring.GetGameFrame()
		boolAiming=false
		return true 
		end
	return false
end


boolFiring=false
function script.AimWeapon1(Heading, pitch)
    --aiming animation: instantly turn the gun towards the enemy
    Signal(SIG_IDLE)
	if boolFiring==false then
	boolFiring=true
        Signal(SIG_AIM2)
        SetSignalMask(SIG_AIM2)
			aimAnimation(Heading,pitch)
		return true
	else	
        return false
    end
end

function delayedStop()
setSpeedEnv(unitID,0.0)
Sleep(1500)
setSpeedEnv(unitID,1.0)
end

teamID = Spring.GetUnitTeam(unitID)
function script.FireWeapon1()
	x,y,z=Spring.GetUnitPosition(unitID)
	Spring.SpawnCEG( "nanofirestart",x,y+10,z, math.random(-10, 10) / 10, math.random(0, 10) / 10, math.random(-10, 10) / 10)
	spawnCegAtPiece(unitID,body,"firecolumn",1)
	StartThread(delayedStop)
    return true
end


function killedAnimation()
	resetAll(unitID)
	Sleep(500)
	mSyncIn(body,0, -6, 5, 1500)
	tSyncIn(LegR,-28,0,0, 1500)
	tSyncIn(LLegR,137,0,0, 1500)	
	tSyncIn(LegL,-28,0,0, 1500)
	tSyncIn(LLegL,137,0,0, 1500)

	tSyncIn(ArmL,-175,0,0, 1500)
	tSyncIn(Head,-21,0,0, 500)
	tSyncIn(ArmR,-175,0,0, 1000)
	for i=1,15 do
		rVal=math.random(-5,5)
		Turn(ArmL,z_axis,math.rad(rVal),12)
		Turn(ArmR,z_axis,math.rad(rVal*-1),12)
		spawnCegAtPiece(unitID,body,"firecolumn",1)
		Sleep(100)
	end
	tSyncIn(ArmL,-175,0,math.random(-90,80), 1000)
	tSyncIn(ArmR,-200,0,math.random(-20,20), 500)
	tSyncIn(center,82,0,0, 1000)
	mSyncIn(body,0, 0,0, 1000)
	tSyncIn(Head,0,75*randSign(),0, 500)
	tSyncIn(LegR,12,0,math.random(-5,5), 1000)
	tSyncIn(LLegR,0,0,0, 1000)	
	tSyncIn(LegL,12,0,math.random(-5,5), 1000)
	tSyncIn(LLegL,0,0,0, 1000)
	spawnCegAtPiece(unitID,body,"firecolumn",1)
	for i=1,6 do
		spawnCegAtPiece(unitID,body,"firecolumn",1)
		Sleep(200)
	end
	tSyncIn(ArmR,-170,0,math.random(-20,20), 500)
	Sleep(750)
end

function script.Killed(recentDamage, maxHealth)
	setSpeedEnv(unitID,0.0)
    Signal(SIG_WALK)
    Signal(SIG_IDLE)
	killedAnimation()


    return 0
end


local function legs_down()
    Move(center, x_axis, 0, 12)
    Move(center, y_axis, 0, 12)
    Move(center, z_axis, 0, 12)
    Move(body, x_axis, 0, 12)
    Move(body, y_axis, 0, 12)
    Move(body, z_axis, 0, 12)
    Turn(center, x_axis, math.rad(0), 15)
    Turn(center, y_axis, math.rad(0), 15)
    Turn(center, z_axis, math.rad(0), 15)
    Turn(body, x_axis, math.rad(0), 15)
    Turn(body, y_axis, math.rad(0), 15)
    Turn(body, z_axis, math.rad(0), 15)
    Turn(LegL, x_axis, math.rad(0), 15)
    Turn(LegR, x_axis, math.rad(0), 15)
    Turn(LLegR, x_axis, math.rad(0), 32)
    Turn(LLegL, x_axis, math.rad(0), 32)
	if boolAiming== false then


    Turn(ArmR, x_axis, math.rad(0), 12)
    Turn(ArmR, y_axis, math.rad(0), 12)
    Turn(ArmR, z_axis, math.rad(0), 12)
    Turn(ArmL, x_axis, math.rad(0), 12)
    Turn(ArmL, y_axis, math.rad(0), 12)
    Turn(ArmL, z_axis, math.rad(0), 12)
	end

    Turn(staff, x_axis, math.rad(0), 3)
    Turn(staff, y_axis, math.rad(0), 13)
    Turn(staff, z_axis, math.rad(0), 3)
    Turn(LegL, z_axis, math.rad(0), 3)
    Turn(LegR, z_axis, math.rad(0), 3)
    Turn(LLegL, z_axis, math.rad(0), 3)
    Turn(LLegR, z_axis, math.rad(0), 3)


    Turn(center, y_axis, math.rad(0), 5)
    Turn(Head, y_axis, math.rad(0), 3)
end

function script.StartMoving()
    -- ----Spring.Echo ("starting to walk!")
    Signal(SIG_IDLE)
    legs_down()
    StartThread(walk)
end

function downHillGlideDetector()

    while true do
        x, y, z = Spring.GetUnitPosition(unitID)
        gx, gy, gz = getUnitMoveGoal(unitID)
        if gx then
            gh = Spring.GetGroundHeight(x, z)
            ggh = Spring.GetGroundHeight(gx, gz)

            if ggh + 10 < gh and ggh > 0 then
                legs_down()
                Turn(staff, y_axis, math.rad(90), 8)
                Turn(ArmL, x_axis, math.rad(-179), 4)
                Turn(ArmR, x_axis, math.rad(-179), 4)
                WaitForTurns(ArmL, ArmR, staff)
                --transform always into a glider downhill
                transformUnitInto(unitID, "jfiredanceglider")
            end
        end
        Sleep(250)
    end
end

function script.StopMoving()

    -- ----Spring.Echo ("stopped walking!")
    Signal(SIG_IDLE)
    Signal(SIG_WALK)
    legs_down()
    StartThread(idle)
end

--FireWeapon2


function stanceReSet()
    Sleep(500)
    Turn(center, y_axis, math.rad(0), 12)
end