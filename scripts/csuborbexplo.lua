include "createCorpse.lua"
include "lib_UnitScript.lua"
include "lib_jw.lua"

center = piece "center"
rotatecenter = piece "rotatecenter"
impactor = piece "impactor"
fire1 = piece "fire1"
fire2 = piece "fire2"
fireballcenter = piece "fireballcenter"
flameemit1 = piece "flameemit1"
flameemit2 = piece "flameemit2"
flameemit3 = piece "flameemit3"
FireLenght = 1024

ring = {}
for i = 1, 12, 1 do
    ring[i] = {}
    temp = "ringparticlespawn" .. i
    ring[i] = piece(temp)
end

local SIG_BLINK = 1
local SIG_FLAME = 2

fireballz = {}
for i = 1, 9, 1 do
    fireballz[i] = {}
    temp = "fireball" .. i
    fireballz[i] = piece(temp)
end

function decalFactoryEmit()
    Sleep(300)
    posX, posY, posZ = Spring.GetUnitPosition(unitID)
    Spring.CreateUnit("NukedecalFactory", posX, posY, posZ, 0, teamID)
end

spiralcenter = piece "spiralcenter"
fireFx = piece "fireFx"
function spawnBrix()
    temp = 25
    max = 600
    local spEmitFx = EmitSfx
    while (temp < 350) do
        Move(fireFx, x_axis, temp, 0)
        temp = temp + 18.4
        for i = 1, 360, 1 do
            holyRandoma = math.random(0, 1)
            if holyRandoma == 1 then
                Turn(spiralcenter, y_axis, math.rad(i), 0, true)
                spEmitFx(fireFx, 1032)
            end
        end
        Turn(spiralcenter, y_axis, math.rad(0), 0, true)
        Sleep(2)
    end
end

function blinkFire()
    Spin(fire1, y_axis, math.rad(52), 0.3)
    Spin(fire2, y_axis, math.rad(-52), 0.3)
    SetSignalMask(SIG_BLINK)
    while (true) do
        deciOne = math.random(0, 2)
        if deciOne == 1 then
            Show(fire1)
        elseif deciOne == 0 then
            Show(fire2)
        else
            Show(fire2)
            Show(fire1)
        end
        Sleep(80)
        Hide(fire1)
        Hide(fire2)
        Sleep(120)
    end
end

function playSound()
    Sleep(750)
    Spring.PlaySoundFile("sounds/csubOrbital/impactor.wav", 1.0)
end

function flameEmit()
    SetSignalMask(SIG_FLAME)
    local spEmitFx = EmitSfx
    while (true) do
        spEmitFx(flameemit1, 1026)
        spEmitFx(flameemit1, 1027)
        spEmitFx(flameemit2, 1026)
        spEmitFx(flameemit2, 1027)
        spEmitFx(flameemit3, 1026)
        spEmitFx(flameemit3, 1027)
        Sleep(50)
    end
end

function spawnFire(Time, x, y, z)
    local spSpawnCEG = Spring.SpawnCEG
    for i = 1, Time, 1 do
        spSpawnCEG("unitonfire", x, y, z, 0, 1, 0, 50, 0)
        spSpawnCEG("unitonfire", x, y + 7, z, 0, 1, 0, 50, 0)

        Sleep(90)
    end

    for i = 1, Time * 3, 1 do
        spSpawnCEG("blackerthensmoke", x, y, z, 0, 1, 0, 50, 0)
        Sleep(90)
    end
end

local abstractUnit = getAbstractTypes(UnitDefNames)
function goTooKillThemAllPicaMon()

    selectRange = 260
    piecePosX, piecePosY, piecePosZ = Spring.GetUnitPosition(unitID)
    --- -Spring.Echo("PiecePosX:",piecePosX.." | PiecePosZ:",piecePosZ)
    -- get Piece Position
    proChoice = Spring.GetUnitsInCylinder(piecePosX, piecePosZ, selectRange) --no idea why 2.9 but satan told me so

    if proChoice ~= nil then

        --Kill the Unit
        for i = 1, table.getn(proChoice), 1 do
            if proChoice[i] ~= unitID then
				 if not abstractUnit[Spring.GetUnitDefID(proChoice[i])] then
                x, y, z = Spring.GetUnitPosition(proChoice[i])
                StartThread(spawnFire, 82, x, y, z)
                Spring.SetUnitNoDraw(proChoice[i], false)
                Spring.DestroyUnit(proChoice[i], false, true) --leave no wreck
            end
            end
        end
    end

    proFeature = Spring.GetFeaturesInCylinder(piecePosX, piecePosZ, selectRange)
    if proFeature ~= nil then

        --Kill the Unit
        for i = 1, table.getn(proFeature), 1 do
            x, y, z = Spring.GetFeaturePosition(proFeature[i])
            StartThread(spawnFire, 82, x, y, z)
            Spring.DestroyFeature(proFeature[i], false, false) --leave no wreck
        end
    end
end

--Reset the Parts
function randomFire(Time)

    x, y, z = Spring.GetUnitPosition(unitID)

    temptime = 0
    local spSpawnCEG = Spring.SpawnCEG
    randx = math.random(-150, 150)
    randz = math.random(-150, 150)
    randx = math.random(-150, 150)
    randz = math.random(-150, 150)
    while (temptime < Time) do


        spSpawnCEG("flames", x + randx, y, z + randz, 0, 1, 0, 50, 0)
        spSpawnCEG("glowsmoke", x + randx, y + 20, z + randz, 0, 1, 0, 80, 0)
        spSpawnCEG("volcanolightsmall", x + randx, y, z + randz, 0, 1, 0, 50, 0)
        Sleep(60)
        temptime = temptime + 60
    end
end

function prepareDeformTable(size, height)
    cent = math.ceil(size / 2)
    T = {}
    for o = 1, size, 1 do
        T[o] = {}
        for i = 1, size, 1 do
            --default
            T[o][i] = 0
            distcent = math.sqrt((cent - i) ^ 2 + (cent - o) ^ 2)

            if distcent < cent - 1 then
                T[o][i] = (cent - distcent) * height
            end
        end
    end

    return T
end

function randomFires()
    nr = math.random(2, 4)
    for i = 1, nr, 1 do
        randomnumber = math.random(12000, 24000)
        StartThread(randomFire, randomnumber)
    end

    while (true) do
        Sleep(100)
    end
end

function volcaniclavalamp()
    local spEmitSFX = EmitSfx
    for i = 1, 42, 1 do
        Sleep(60)
        spEmitSFX(center, 1031)
    end
end

function justWaitAndSee()
    Hide(impactor)

    Turn(center, y_axis, math.rad(180), 0, true)

    posX, posY, posZ = Spring.GetUnitPosition(unitID)
    teamID = Spring.GetUnitTeam(unitID)
    --set the unit to no collide
    Spring.SetUnitNoSelect(unitID, true)
    Spring.SetUnitNeutral(unitID, true)
    Spring.SetUnitBlocking(unitID, false)
	delay= math.random(0,500)
	Sleep(delay)
    --set the unit to be ignored
    Show(impactor)
    StartThread(blinkFire)
    Turn(impactor, z_axis, math.rad(-90), 0)
    StartThread(flameEmit)
    StartThread(playSound)
    Turn(impactor, z_axis, math.rad(0), 0.41083005)
    WaitForTurn(impactor, z_axis)
    size = 20
    x, y, z = Spring.GetUnitPosition(unitID)
    if GG.DynDefMap == nil then GG.DynDefMap = {} end
    if GG.DynRefMap == nil then GG.DynRefMap = {} end
    GG.DynDefMap[#GG.DynDefMap + 1] = { creator=UnitDefs[Spring.GetUnitDefID(unitID)].name,x = x / 8, z = z / 8, Size = size, blendType = "sub", filterType = "borderblur" }
    GG.DynRefMap[#GG.DynRefMap + 1] = prepareDeformTable(240, -2)
    GG.boolForceLandLordUpdate = true


    Spring.CreateUnit("NukedecalFactory", posX, posY, posZ, 0, teamID)
    Hide(impactor)
    goTooKillThemAllPicaMon()
    EmitSfx(center, 1028)
    Signal(SIG_BLINK)
    Signal(SIG_FLAME)
    Hide(fire1)
    Hide(fire2)

    EmitSfx(fire1, 1024)
    Sleep(10)
    EmitSfx(center, 1024)
    StartThread(spawnBrix)
    for i = 1, 7, 1 do
        EmitSfx(center, 1025) --sphere
    end
    Sleep(50)
    EmitSfx(center, 1029)
    Sleep(400)
    EmitSfx(center, 1025) --sphere
    EmitSfx(center, 1029)

    ox, y, oz = Spring.GetUnitPosition(unitID)
    if not GG.AddFire then GG.AddFire = {} end
    for i = 1, 4 do

        GG.AddFire[#GG.AddFire + 1] = { x = ox + math.random(-50, 50), z = oz + math.random(-50, 50), Food = FireLenght }
    end
    Sleep(2000)
    StartThread(volcaniclavalamp)
    StartThread(randomFires)
    Sleep(20000)

    Spring.DestroyUnit(unitID, false, true)
end

function script.Create()
    StartThread(justWaitAndSee)
end

function script.Killed()
end