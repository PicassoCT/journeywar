include "lib_OS.lua"
include "lib_UnitScript.lua"
include "lib_Animation.lua"

include "lib_jw.lua"
include "lib_Build.lua"

local spinAroundSpot = piece "spinAroundSpot"

treeSpot = piece "treeSpot"
local mdwheelcenter = piece "mdwheelcenter"
local mdwheell = piece "mdwheell"
local mdwheelr = piece "mdwheelr"
local mdiggroup = piece "mdiggroup"
local mdmeltingp = piece "mdmeltingp"
local sparkemit1 = piece "sparkemit1"
local spinroundPoint = piece "spinroundpoint"
local mdigg = piece "mdigg"
local SIG_WIP = 2
local SIG_GIC = 4
local SIG_MOVE = 8
local SIG_MINE = 16
local smokepiece = piece "chineySmokeEmit"
local mdprojecti = piece "mdprojecti"

local heat1emit = piece "heat1emit"
local heat2emit = piece "heat2emit"
local heat3emit = piece "heat3emit"
local heat4emit = piece "heat4emit"
local heat5emit = piece "heat5emit"
local heat6emit = piece "heat6emit"

dustEmit = {}
for i = 1, 8, 1 do
    dustEmit[i] = {}
    dusty = "dustemit" .. i
    dustEmit[i] = piece(dusty)
end

local boolAllreadyDead = false
local boolMurdered = false
local boolAllreadyDeployed = false
mexID = -666
local boolCheckActive = false
local boolMoving = false
boolDeploy = false
boolShortStop = false
boolLongStop = false
boolOneThreadOnly = false

--Created

function script.Activate()
	 Signal(SIG_WIP)
    boolDeploy = true
	 StartThread( makeMex)
    return 1
end

function script.Deactivate()

    boolDeploy = false

    return 0
end



local teamID = Spring.GetUnitTeam(unitID)
--gets the Units Position, creates the Mex
function makeMex()
		transformUnitInto(unitID, "mdiggMex")
end

function dirtEmit()
    for i = 1, 8, 1 do
        EmitSfx(dustEmit[i], 1024)
        Sleep(15)
        EmitSfx(dustEmit[9 - i], 1024)
    end
end

function sparkEmit()
    EemitX, EemitY, EemitZ = Spring.GetUnitPiecePosition(unitID, sparkemit1)
    EmitSfx(sparkemit1, 1025)
end




function workInProgress()
    Signal(SIG_WIP)
    SetSignalMask(SIG_WIP)
    while (true) do
        Spin(mdwheelcenter, x_axis, math.rad(-40), 9)
        Spin(mdwheell, y_axis, math.rad(-40), 9)
        Spin(mdwheelr, y_axis, math.rad(40), 9)
        Turn(mdmeltingp, y_axis, math.rad(-55), 3)
        WaitForTurn(mdmeltingp, y_axis)
        unitX, unitY, unitZ = Spring.GetUnitPosition(unitID)
        CemitX, CemitY, CemitZ = Spring.GetUnitPiecePosition(unitID, smokepiece)
        Spring.SpawnCEG("fireSparks", CemitX + unitX, CemitY + unitY, CemitZ + unitZ, 0, 1, 0, 50, 0)
        EmitSfx(smokepiece, 258)
        StartThread(dirtEmit)
        Sleep(120)
        EmitSfx(smokepiece, 258)
        --DustEmits?
        Turn(mdmeltingp, y_axis, math.rad(-120), 3)
        WaitForTurn(mdmeltingp, y_axis)
        EmitSfx(smokepiece, 258)
        sparkEmit()
        StartThread(dirtEmit)
        Sleep(120)
        EmitSfx(smokepiece, 258)
        Turn(mdmeltingp, y_axis, math.rad(-180), 3)
        WaitForTurn(mdmeltingp, y_axis)
        EmitSfx(smokepiece, 258)
        sparkEmit()
        StartThread(dirtEmit)
        Sleep(120)
        EmitSfx(smokepiece, 258)
        Turn(mdmeltingp, y_axis, math.rad(-230), 3)
        WaitForTurn(mdmeltingp, y_axis)
        EmitSfx(smokepiece, 258)
        sparkEmit()
        StartThread(dirtEmit)
        Sleep(120)
        EmitSfx(smokepiece, 258)
        Turn(mdmeltingp, y_axis, math.rad(-300), 3)
        WaitForTurn(mdmeltingp, y_axis)
        EmitSfx(smokepiece, 258)
        sparkEmit()
        StartThread(dirtEmit)
        Sleep(120)
        EmitSfx(smokepiece, 258)
        Turn(mdmeltingp, y_axis, math.rad(-360), 3)
        WaitForTurn(mdmeltingp, y_axis)
        EmitSfx(smokepiece, 258)
        sparkEmit()
        StartThread(dirtEmit)
        Sleep(120)
    end
end

function killMex()
    if (Spring.ValidUnitID(mexID)) then
        Spring.DestroyUnit(mexID, false, true) --leave no wreck
        boolOneThreadOnly = false
        Show(mdigg)
        Show(mdwheelcenter)
        Show(mdwheell)
        Show(mdwheelr)
        Show(mdiggroup)
        Show(mdmeltingp)
    end
end



function Killed()

    Signal(SIG_WIP)

    Spin(mdwheelcenter, x_axis, math.rad(40), 4)
    Turn(spinroundPoint, y_axis, math.rad(-80), 1)

    WaitForTurn(spinroundPoint, y_axis)
    Turn(spinroundPoint, y_axis, math.rad(-120), 1)

    WaitForTurn(spinroundPoint, y_axis)
    Turn(mdwheelcenter, z_axis, math.rad(7), 4)
    Turn(spinroundPoint, y_axis, math.rad(-180), 1)

    WaitForTurn(spinroundPoint, y_axis)

    Turn(spinroundPoint, y_axis, math.rad(-287), 1)
    Sleep(800)

    WaitForTurn(spinroundPoint, y_axis)
    Explode(heat1emit, SFX.FIRE + SFX.FALL)
    Sleep(140)
    Explode(heat2emit, SFX.FIRE + SFX.FALL)
    Explode(heat3emit, SFX.FIRE + SFX.FALL)
    Sleep(180)
    Explode(heat4emit, SFX.FIRE + SFX.FALL)
    Explode(heat5emit, SFX.FIRE + SFX.FALL)
    Explode(heat6emit, SFX.FIRE + SFX.FALL)
    Sleep(190)
    Explode(mdwheelcenter, SFX.FIRE + SFX.FALL)
    Explode(mdigg, SFX.SHATTER)
    Explode(heat1emit, SFX.FIRE + SFX.FALL)
    Explode(heat2emit, SFX.FIRE + SFX.FALL)
    Explode(heat3emit, SFX.FIRE + SFX.FALL)
    Explode(heat4emit, SFX.FIRE + SFX.FALL)
    Explode(heat5emit, SFX.FIRE + SFX.FALL)
    Explode(heat6emit, SFX.FIRE + SFX.FALL)
    Sleep(800)
    Hide(mdwheelcenter)
end

function script.StartMoving()
    boolMoving = true
    Signal(SIG_MINE)
    killMex()


    --- -Spring.Echo("StartThread(workInProgress)")
    StartThread(workInProgress)


    boolShortStop = false
end

function script.StopMoving()
    boolMoving = false

    --- -Spring.Echo("Stoped Moving")
    boolShortStop = true
    Signal(SIG_WIP)
    StopSpin(mdwheelcenter, x_axis)
    StopSpin(mdwheell, y_axis)
    StopSpin(mdwheelr, y_axis)
    --- -Spring.Echo("StartThread(delayedCheck)")
end

--Killed

function script.Create()
    StartThread(treeTrample)

    Hide(mdprojecti)
    --- -Spring.Echo("I speak, therefore I exist.")
end


------------------------------------------------------------------------------------------
function treeTrample()
    treeTypeTable = getTypeTable(UnitDefNames, {
        "jtree1",
        "jtree2",
        "jtree3",
        "jtree41",
        "jtree42",
        "jtree43",
        "jtree44",
        "jtree45",
        "jtree46",
        "jtree47"
    })
    while true do

        while boolMoving == true do
            x, _, z = Spring.GetUnitPiecePosDir(unitID, treeSpot)
            T = getAllInCircle(x, z, 50, unitID, teamID)
            if T then
                T = getUnitsOfTypeInT(T, treeTypeTable)

                if T and #T > 0 then
                    GG.TreesTrampled = true

                    if not GG.TableTreesTrampled then GG.TableTreesTrampled = {} end

                    for i = 1, #T do
                        GG.TableTreesTrampled[T[i]] = true
                    end
                end
            end
            Sleep(150)
        end
        Sleep(300)
    end
end

