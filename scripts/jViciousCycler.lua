center = piece "center"

function script.Create()
end

function script.Killed(recentDamage, maxHealth)
end

--- -take off & landing animation
function script.Activate()
    --take off animation: spin rotor quickly and unfold
end

function script.Deactivate()
    --landing animation: slow down the the rotor
end


function script.StopBuilding()
end

function script.StartBuilding(heading, pitch)
end

Spring.SetUnitNanoPieces(unitID, { center })
