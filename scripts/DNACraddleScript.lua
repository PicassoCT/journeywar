local piecesTable = {}
center = piece "center"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = center
fclvl23DS = piece "fclvl2.3DS"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = fclvl23DS
Kreis02 = piece "Kreis02"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = Kreis02
LegCenter1 = piece "LegCenter1"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LegCenter1
UpMain01 = piece "UpMain01"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpMain01
UpJoin02 = piece "UpJoin02"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpJoin02
LoJoin02 = piece "LoJoin02"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LoJoin02
UpJoin01 = piece "UpJoin01"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpJoin01
LoJoin01 = piece "LoJoin01"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LoJoin01
LegCenter2 = piece "LegCenter2"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LegCenter2
UpMain02 = piece "UpMain02"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpMain02
UpJoin03 = piece "UpJoin03"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpJoin03
LoJoin03 = piece "LoJoin03"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LoJoin03
UpJoin04 = piece "UpJoin04"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpJoin04
LoJoin04 = piece "LoJoin04"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LoJoin04
LegCenter3 = piece "LegCenter3"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LegCenter3
UpMain03 = piece "UpMain03"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpMain03
UpJoin05 = piece "UpJoin05"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpJoin05
LoJoin05 = piece "LoJoin05"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LoJoin05
UpJoin06 = piece "UpJoin06"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpJoin06
LoJoin06 = piece "LoJoin06"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LoJoin06
LegCenter4 = piece "LegCenter4"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LegCenter4
UpMain04 = piece "UpMain04"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpMain04
UpJoin07 = piece "UpJoin07"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpJoin07
LoJoin07 = piece "LoJoin07"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LoJoin07
UpJoin08 = piece "UpJoin08"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpJoin08
LoJoin08 = piece "LoJoin08"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LoJoin08
LegCenter5 = piece "LegCenter5"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LegCenter5
UpMain05 = piece "UpMain05"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpMain05
UpJoin09 = piece "UpJoin09"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpJoin09
LoJoin09 = piece "LoJoin09"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LoJoin09
UpJoin010 = piece "UpJoin010"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpJoin010
LoJoin010 = piece "LoJoin010"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LoJoin010
LegCenter6 = piece "LegCenter6"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LegCenter6
UpMain06 = piece "UpMain06"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpMain06
UpJoin011 = piece "UpJoin011"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpJoin011
LoJoin011 = piece "LoJoin011"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LoJoin011
UpJoin012 = piece "UpJoin012"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = UpJoin012
LoJoin012 = piece "LoJoin012"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = LoJoin012
geneCircle = piece "geneCircle"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = geneCircle
gene01 = piece "gene01"
genestart = #piecesTable + 1
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene01
gene02 = piece "gene02"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene02
gene03 = piece "gene03"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene03
gene04 = piece "gene04"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene04
gene05 = piece "gene05"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene05
gene06 = piece "gene06"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene06
gene07 = piece "gene07"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene07
gene08 = piece "gene08"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene08
gene09 = piece "gene09"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene09
gene10 = piece "gene10"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene10
gene11 = piece "gene11"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene11
gene12 = piece "gene12"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene12
gene13 = piece "gene13"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene13
gene14 = piece "gene14"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene14
gene15 = piece "gene15"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene15
gene16 = piece "gene16"
geneend = #piecesTable + 1
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = gene16
crystal = piece "crystal"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = crystal
Kreis08 = piece "Kreis08"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = Kreis08
Kreis09 = piece "Kreis09"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = Kreis09
Kreis10 = piece "Kreis10"
piecesTable[#piecesTable + 1] = {}
piecesTable[#piecesTable] = Kreis10

birthplace = piece "birthplace"
function resetedByBuilding()
    SetSignalMask(SIG_RESET)
    --- -Spring.Echo("reseter Triggered")
    Sleep(3000)
    boolreVert = true
end


legTable = {}
for i = 1, 6, 1 do
    legTable[i] = {}
    piecename = "UpMain0" .. i
    legTable[i] = piece(piecename)
end
SIG_LIFE = 1
SIG_RESET = 2
SIG_WATER = 4


function waitingGame()
    while (true) do
        --- -Spring.Echo("Alive")
        ---- Spring.Echo("BoolRevert",boolreVert)
        if boolreVert == true then
            Move(Kreis02, y_axis, 0, 8)
            WaitForMove(Kreis02, y_axis)
            local x, y, z = Spring.GetUnitPosition(unitID)
            local teamID = Spring.GetUnitTeam(unitID)
            mexID = Spring.CreateUnit("jmfactorytwo", x, y, z, 0, teamID)
            health = Spring.GetUnitHealth(unitID)
            Spring.SetUnitHealth(mexID, health)
            Spring.DestroyUnit(unitID, false, true)
        end








        Sleep(1000)
    end
end

--Killed
function script.QueryBuildInfo()
    return birthplace
end



geneval = genestart
function script.QueryNanoPiece()
    geneval = geneval + 1
    if geneval > geneend then geneval = genestart end
    return piecesTable[geneval]
end

function script.Activate()



    SetUnitValue(COB.YARD_OPEN, 1)
    SetUnitValue(COB.INBUILDSTANCE, 1)
    SetUnitValue(COB.BUGGER_OFF, 1)

    return 1
end

function soundDuringBuild()

    while true do
        Sleep(500)
        while boolbuild == true do
            dax = math.random(1, 8)
            loudness = math.random(0.8, 1)
            if math.random(0, 1) == 1 then
                for i = 1, dax do
                    Spring.PlaySoundFile("sounds/jFactory/Factory1.wav", (loudness + i / 10) % 1)
                    Sleep(4000)
                end
            else
                for i = 1, dax do
                    Spring.PlaySoundFile("sounds/jFactory/Factory3.wav", (loudness + i / 10) % 1)
                    Sleep(4000)
                end
            end
            ceceil = math.ceil(math.random(200, 1900))
            Sleep(ceceil)
        end
    end
end


function script.Deactivate()





    SetUnitValue(COB.YARD_OPEN, 0)
    SetUnitValue(COB.INBUILDSTANCE, 0)
    SetUnitValue(COB.BUGGER_OFF, 0)
    return 0
end

function waterGames()
    SetSignalMask(SIG_WATER)
    while true do

        EmitSfx(birthplace, 1025)
        Sleep(150)
    end
end

function liftFeet()
    Turn(feetFetish[1], z_axis, math.rad(19), 6.9)
    Turn(feetFetish[2], z_axis, math.rad(-19), 6.9)

    Turn(feetFetish[3], x_axis, math.rad(-10), 6.9)
    Turn(feetFetish[3], z_axis, math.rad(19), 6.9)

    Turn(feetFetish[4], x_axis, math.rad(19), 6.9)
    Turn(feetFetish[4], z_axis, math.rad(0.5), 6.9)

    Turn(feetFetish[5], x_axis, math.rad(-11), 6.9)
    Turn(feetFetish[5], z_axis, math.rad(-19), 6.9)

    Turn(feetFetish[6], x_axis, math.rad(10), 6.9)
    Turn(feetFetish[6], z_axis, math.rad(19), 6.9)
end

function isBuilding()
    while (true) do
        if boolMightBeAReset == true then
            boolMightBeAReset = false
            StartThread(resetedByBuilding)
        end


        if Spring.GetUnitIsBuilding(unitID) ~= nil then

            --- -Spring.Echo("ReseterOffLine")
            boolMightBeAReset = false
        end

        Sleep(150)
    end
end

boolbuild = false

function script.StartBuilding()
    boolbuild = true
    Signal(SIG_WATER)
    Signal(SIG_LIFE)
    StartThread(circleOfLife)
    StartThread(waterGames)
    --animation
    Signal(SIG_RESET)
end

function script.StopBuilding()
    boolbuild = false
    --animation
    Signal(SIG_WATER)
    Signal(SIG_LIFE)
    for i = genestart, geneend, 1 do
        Hide(piecesTable[i])
    end
    --- -Spring.Echo("ReseterOffLine")

    boolMightBeAReset = true
end

function circleOfLife()
    for i = genestart, geneend, 1 do
        Show(piecesTable[i])
    end

    SetSignalMask(SIG_LIFE)
    Move(geneCircle, y_axis, -41, 0)
    Move(geneCircle, y_axis, 0, 5)
    WaitForMove(geneCircle, y_axis)
    while true do
        Move(geneCircle, y_axis, -11, 0)
        Show(gene14)
        Show(gene07)
        Show(gene13)
        Show(gene06)


        Move(geneCircle, y_axis, 0, 5)
        Sleep(650)
        Hide(gene14)
        Hide(gene07)
        WaitForMove(geneCircle, y_axis)
        Sleep(150)
        Hide(gene13)
        Hide(gene06)
    end
end

function script.Create()
    Spin(crystal, y_axis, math.rad(42), 6)
    Spin(geneCircle, y_axis, math.rad(-42), 6)
    x = 0
    Move(Kreis02, y_axis, -39, 18)
    Turn(LegCenter1, y_axis, math.rad(0), 0)
    Turn(LegCenter2, y_axis, math.rad(60), 0)
    Turn(LegCenter3, y_axis, math.rad(120), 0)
    Turn(LegCenter4, y_axis, math.rad(180), 0)
    Turn(LegCenter5, y_axis, math.rad(240), 0)
    Turn(LegCenter6, y_axis, math.rad(300), 0)

    StartThread(waitingGame)
    StartThread(soundDuringBuild)
    for i = genestart, geneend, 1 do
        Hide(piecesTable[i])
    end
end

