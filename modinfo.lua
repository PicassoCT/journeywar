local modinfo = {
	name = "Journeywar",
	shortname = "JW",
	game = "Journeywar",
	shortgame = "JW",
	description = "Combine vs Exobiotics ",
	url = "http://springrts.com/phpbb/viewtopic.php?f=69&t=23275",
	version = '$VERSION',
	modtype = "1",
	--Perfectionism is the death of good things
	depend = {
		"cursors.sdz",
		"Spring content v1"
	}
}

return modinfo
