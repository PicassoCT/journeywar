local unitName = "jsempresequoia"

local unitDef = {
	name = "Sequoia ",
	Description = "ground dominating Tree Ship <Air Fortress Unit>",
	objectName = "jsequoia.s3o",
	script = "sequoiascript.lua",
	buildPic = "jsempresequoia.png",
	--cost
	buildCostMetal = 10000,
	buildCostEnergy = 5000,
	buildTime =1,
	--Health
	maxDamage = 44000,
	idleAutoHeal = 1,
	--Movement
	showNanoFrame=false,
	FootprintX = 5,
	FootprintZ = 5,
	
	MaxVelocity = 15,
	MaxWaterDepth =15,
	--MovementClass = "Default2x2",--
	
	selfDestructAs ="defaultweapon",
	explodeAs = "defaultweapon",
	airstrafe= true,
	strafeToAttack=true,
	TurnRate= 125,
	sightDistance = 525,
	onOffable = true,
	ActivateWhenBuilt=false,
	reclaimable=true,
	CanAttack = true,
	CanGuard = true,
	CanMove = true,
	CanPatrol = true,
	CanStop = true,
	LeaveTracks = false,
	-- Building	
		turnInPlace=true,
	Category = [[AIR]],
	usePieceCollisionVolumes = true,
	
	cruiseAlt=170,
	CanFly = true,
	hoverAttack =false,
	maxBank=0.1,
	myGravity =0.5,
	mass = 1225,
	canSubmerge = true,
	useSmoothMesh =true,
	crashDrag =0.1,
	collide =true,
	airHoverFactor =0,
	dontLand		 	= true,
	HoverAttack=true,
	verticalSpeed= 1,
	factoryHeadingTakeoff =false,
	MovementClass = "Default2x2",
	MaxWaterDepth=15,
	-------------------------------
	
	ActivateWhenBuilt=1;
	BuildTime=51000;
	Corpse="",
	
	RadarDistance=0,
	SightDistance=800,
		steeringmode = [[1]],
	maneuverleashlength = 1380,
	turnRadius		 	= 16,
	--------------------------------
	Acceleration = 0.5,
	--turnRadius		 	= 8,
	
	weapons = {
		
		[2]={name = "jtreelazor",
			onlyTargetCategory = [[LAND]],
		},
		[3]={name = "jtreelazor",
			onlyTargetCategory = [[LAND]],
		},
		[4]={name = "jtreelazor",
			onlyTargetCategory = [[LAND]],
		},
		[1]={name = "jtreelazor",
			onlyTargetCategory = [[LAND]],
		},
		[5]={name = "greenseer",
			onlyTargetCategory = [[LAND]],
		},	
	},
	
	
	customParams = {},
	sfxtypes = {
		explosiongenerators = {
			"custom:dirt",
			"custom:leaves", --1025
			"custom:sequoiawarp", --1026
			--
			
		},
	}
	
	------------------------
	
	
	
	
}

return lowerkeys({ [unitName] = unitDef })