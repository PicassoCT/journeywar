local unitName = "chunter"

local unitDef = {
	name = "Huntersynth",
	Description = "advanced close-combat Warrior <Assault Unit>",
	objectName = "jHunter.s3o",
	script = "jhunterscript.lua",
	buildPic = "chunter.png",
	--cost
	buildCostMetal = 3500,
	buildCostEnergy = 1200,
	buildTime = 80,
	--Health
	maxDamage = 3200,
	idleAutoHeal = 2,
	--Movement
	Acceleration = 0.25,
	BrakeRate = 0.3,
	FootprintX = 2,
	FootprintZ = 2,
	MaxSlope = 80,
	MaxVelocity = 1.6,
	MaxWaterDepth = 65,
	MovementClass = "Default2x2",
	TurnRate = 1250,

	sightDistance = 900,
	upright=false,
	onoffable=true,
	activateWhenBuilt =false,
	CanAttack = true,
	CanGuard = true,
	CanMove = true,
	CanPatrol = true,
	CanStop = true,
	LeaveTracks = true, 

	Builder = false,
	ShowNanoSpray = true,
	CanBeAssisted = false,
	CanReclaim=false,	

		explodeAs = [[NOWEAPON]],
		selfDestructAs= [[NOWEAPON]], 
	 
	 
	Category = [[LAND]],

	weapons = {
	[1]={name  = "jdartgun",
		onlyTargetCategory = [[LAND]],
		mainDir=[[0 0 1]],--nerfed
		maxAngleDif        = 90,--nerfed
		},
},
}

return lowerkeys({ [unitName] = unitDef })