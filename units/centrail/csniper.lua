local unitName = "csniper"

local unitDef = {
	name = "Safarisniper",
	Description = "deployed Sharpshooter - can drag harpooned trophys to base <Long Range Ground Unit/ Recycleable Transporter>",
	objectName = "csniper.s3o",
	script = "csniper.lua",
	buildPic = "csniper.png",
	--cost
	buildCostMetal = 600,
	buildCostEnergy = 250,
	buildTime = 5,
	--Health
	maxDamage = 380,
	idleAutoHeal = 0,
	--Movement
	Acceleration = 0.1,
	BrakeRate = 0.3,
	FootprintX = 2,
	FootprintZ = 2,
	MaxSlope = 5,
	MaxVelocity = 2.5,
	MaxWaterDepth = 20,
	MovementClass = "Default2x2",
	TurnRate = 900,

	nanocolor=[[0 0.9 0.9]],
	collisionvolumetype  = "box",
	collisionvolumescales = "30 65 30",
	collisionvolumeoffsets = "0 0 0",
	 --collisionVolumeTest = 1;
		explodeAs="citadelldrone",
		selfDestructAs="cartdarkmat", 
	moveState=0,
	sightDistance = 650,

	Builder = false,
	CanAttack = true,
	CanGuard = true,
	CanMove = true,
	CanPatrol = true,
	CanStop = true,
	LeaveTracks = true, 
	trackType ="BIGFoot",
	 trackStrength=4,
	trackWidth =48,
	trackOffset =0,
	isFirePlatform=false,
	holdSteady =true,
	releaseHeld =true,
	transportCapacity   = 1,
	transportSize       = 90000,
	  
	  
	
		  sfxtypes            = {	
			explosiongenerators = {
			"custom:smallblueburn",
			"custom:bloodspray",
			"custom:dirt",
			"custom:greenlight",
			"custom:sniperlazzor"
			},	
			},
	Category = [[LAND]],

	weapons = {
	[1]={name  = "sniperweapon",
		onlyTargetCategory = [[LAND]],
		}

	},
}

return lowerkeys({ [unitName] = unitDef })