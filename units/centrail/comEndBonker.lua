local unitName = "comendbonker"

local unitDef = {
maxdamage=650,
  name               = "Comender Mech Hangar",
  description = "places you in a Mechanized Suit, if you agree twice < Hero Unit Construction Building>",
  objectName         = "comendbonker.s3o",
  	buildPic = "comEndBonker.png",
    script = "comEndBonker.lua",
  EnergyStorage = 0,
	EnergyUse = 210,
	MetalStorage = 0,
	EnergyMake = 0, 
	MakesMetal = 0, 
	MetalMake = 0,	
	onoffable = true,
	 corpse             = "bgcorpse",
	buildCostMetal =  1200,
	buildCostEnergy = 900,
	buildTime =82,
	levelground=false,
	category = [[LAND BUILDING]],
	explodeAs="cbonkerplasma",
	selfDestructAs="cartdarkmat",
	FootprintX = 6,
	FootprintZ = 6,
	MaxSlope = 10,	
	activateWhenBuilt=true,
	shownanoframe=1,
	--TransportByEnemy=1;   
	--immunetoparalyzer=1;
	--CantBeTransported=0;
	nanocolor=[[0 0.9 0.9]],
	  yardmap="oooooo oooooo oooooo oooooo oooooo oooooo",
	
	 customParams = {},
 sfxtypes = {
				explosiongenerators = {
				   "custom:dirt",     
				   "custom:weldArc"
				     --electric Sparc
				     --electric Sparc
				   
									},
				},
  


	
	
}

return lowerkeys({ [unitName] = unitDef })