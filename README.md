![alt text](https://springrts.com/phpbb/download/file.php?mode=view&id=6195&sid=6c17b02899467752c524eb54a90f38cc)

A SciFi RTS Game in a HalfLife Paralleluniverse

Centrail Confederacy vs Journeyman
================================================================================================================================

Remember how geared up the Combine of the Half-Life Universe where? 

You are about to find out what they were fighting and what was pushing them too such extreme measures as they took on a world such as ours in seven hours. 

Pick your side citizen -become either a Planetary Administrator fighting the exobiotics for a decadent society - or go wild 
and join the mysterious Journeyman, a transdimensional, biotech glider-gunsociety seeding strange life throughout the unionsverses.

The darkness between the stars, is not empty- its a forrest of dark leaves, growing towards us.

A life wasted for a way to clever RTS.

SETUP 
================================================================================================================================
1. Engine Installing
You need the Spring-Engine. Get and install it at 
http://springrts.com/Downloads

2. Clone the Repository into a folder called journeywar.sdd in your Spring_Engine_Path/games/ folder.

3. Release the Meteor, pardon, Launch the Lobby. Enter the Battleroom. 

4. Configure your game.

5. Enjoy your stay.

If you have questions the author and other gamedevs can answer, contact us via IRC in moddev.


Repository Content
================================================================================================================================

If you want to modify this Game and want a overview of the content.

Here is what you have:

 Foldername:                            Purpose:
/object3d/                              The 3d-Files UnitGraphics are made off.

/scripts  /                              The lua-scripts that define unitbehaviour.

/bitmaps  /                              The Pictures needed for Explosions, Sfx and Footprints

/Features  /                               Everything dead and unmoving on the battlefield is defined here

/gamedata  /                               Contains the files that actually define the explosions behaviours

/LuaRules  /                              Contains small SoftwareTools that interact directly with the GameWorld, defining Rules and Behaviour
/LuaUI    /                                Contains the files that define via the ChilliFrameWork the User Interface

/LUPS       /                               Is yet another helper Library, allowing to use advanced special effects

/music      /                              Contains the music files. No duh.

/sidepics   /                               Contains the Sides Logos

/unitpics  /                              Then Units little buildpictures

/sounds     /                              All the units sounds

/weapons    /                              Contains the weapondefinitions

/units      /                                Contains the unitdefinitions. Go here to modify there values.

/unittextures  /                              Contains the Textures for the 3d Models


Programs needed to open the source:
A 3D Editor for Models, like Blender

A Graphics Software like GIMP

A Sound Editor like Audacity

A texteditor like Notepad++

Many things that are used in the games scripts, are actually helping hands by the engine. 

Which means, you will have to look it up on the Wikki.

Which is work, which can be frustrating and boring.

Which is why you are here, and with me so far.

http://springrts.com/wiki/Main_Page
